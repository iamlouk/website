#!/usr/bin/python3

import os, sys, subprocess
import yaml

MARKDOWN_DIR = './markdown'
WWW_DIR = './www'
TEMPLATE = './my-template.html'

extra_metadata = dict()

def get_metadata(dirpath, metadata=None):
    if metadata is None:
        metadata = dict()

    for name in os.listdir(dirpath):
        fullpath = f'{dirpath}/{name}'
        if name.endswith('.md'):
            url_path = fullpath.replace(MARKDOWN_DIR, '', 1)[:-3] + '.html'
            with open(fullpath, 'r') as f:
                content = f.read()
                if content.startswith('---\n'):
                    splited = content.split('---\n', 2)
                    metadata[url_path] = yaml.safe_load(splited[1])
                else:
                    metadata[url_path] = { "hidden": True, "title": name[:-3] }
                    extra_metadata[fullpath] = metadata[url_path]
        elif os.path.isdir(fullpath):
            get_metadata(fullpath, metadata)
        else:
            print(f'unkown file type or extension: {fullpath}', file=sys.stderr)
            sys.exit(1)

    return metadata


def md_to_html(dirpath, navhtml, clean=False):
    for name in sorted(os.listdir(dirpath)):
        fullpath = f'{dirpath}/{name}'
        if name.endswith('.md'):
            www_path = fullpath.replace(MARKDOWN_DIR, WWW_DIR, 1)[:-3] + '.html'
            if clean is False:
                print(f'{fullpath} -> {www_path}')
                cmd = [
                    'pandoc',
                    '--mathjax', '--from', 'markdown', '--to', 'html',
                    '-V', navhtml,
                    '--template', TEMPLATE,
                    fullpath, '--output', www_path
                ]
                if fullpath in extra_metadata:
                    for key, value in extra_metadata[fullpath].items():
                        cmd.append(f'--metadata={key}:{value}')
                subprocess.run(cmd, check=True)
            else:
                print(f'{www_path} -> delete!')
                os.remove(www_path)
        elif os.path.isdir(fullpath):
            www_path = fullpath.replace(MARKDOWN_DIR, WWW_DIR, 1)
            if clean is False:
                if not os.path.exists(www_path):
                    os.mkdir(www_path)

            md_to_html(fullpath, navhtml, clean)

            if clean is True and os.path.exists(www_path) and len(os.listdir(www_path)) == 0:
                os.rmdir(www_path)
        else:
            print(f'unkown file type or extension: {fullpath}', file=sys.stderr)
            sys.exit(1)

if '--clean' in sys.argv:
    md_to_html(MARKDOWN_DIR, None, clean=True)
    sys.exit(0)

if '--help' in sys.argv:
    print("""usage: ./build.py [--clean | --help]""")
    sys.exit(0)

if len(sys.argv) != 1:
    sys.exit(1)

metadata = get_metadata(MARKDOWN_DIR)
navhtml = ['<ol>']

for url, data in metadata.items():
    if 'hidden' not in data or data['hidden'] is not True:
        title = data['title']
        navhtml.append(f'<li><a href="{url}">{title}</a></li>')

navhtml.append('</ol>')
navhtml = '\n'.join(navhtml)

md_to_html(MARKDOWN_DIR, f'nav={navhtml}')
