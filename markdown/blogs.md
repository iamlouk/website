---
title: Blogs I like
author: Lou Knauer
date: 30.10.2022
faicon: fa-rss
keywords:
  - blog
  - stuff
---

# Some Blog(-Posts) and/or Other Resoruces I Like(d)

Instead of starting a blog myself that nobody would want to read anyways, here I will list some blogs and blogsposts and I like. This will also serve as a kind of personal reminder and bookmark collection. I have no affiliation at all to any of the linked websites. Some other links to blog-like content on the web might also end up here.

- [Eli Bendersky's Website](https://eli.thegreenplace.net/archives/all): I found this website because it contains one of the very very few examples of how to work with DWARF debug information and stayed for the posts about compiler stuff.
- [surma.dev](https://surma.dev/): Some cool posts about WASM and web-stuff. I found this blog after watching the youtube show *HTTP 203*.
- [ThePHD](https://thephd.dev/): C, C++, and more. Great writing style!
- [Lei.Chat(): Compilers and IRs](https://www.lei.chat/posts/compilers-and-irs-llvm-ir-spirv-and-mlir/): Too be honest, I had trouble understanding what exactly MLIR is and why it exists, this series of articles helped me understand it.
- [Paper: Generalization of Conway's "Game of Life" to a continuous domain](https://arxiv.org/abs/1111.1567)
- *...more to come*

