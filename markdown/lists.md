---
title: Lists of Awesome Stuff
author: Lou Knauer
date: 30.11.2020
faicon: fa-list
hidden: True
keywords:
  - lists
  - awesome
  - books
---

As everything on this website, those lists are very incomplete.
They have no particular order. __TODO__: Finish those lists!

# Books (mostly german)
- Cambridge Five - Zeit der Verräter (Hannah Coler)
- Paradox - Am Abgrund der Ewigkeit (Phillip P. Peterson)
- Wayfarers Series (Becky Chambers)
  1. The Long Way to a Small, Angry Planet
  2. A Closed and Common Orbit
  3. Record of a Spaceborn Few
- Marc Elsberg
  1. Blackout
  2. Zero
  3. Helix
  4. Gier
- Childhoods End (Arthur C. Clarc)
- Dune - Der Wüstenplanet (Frank Herbert)

# TV Series
- Drama/Action:
  - Suburra (2017)
  - La Case de Papel (2017)
  - Cannabis (2016)
  - UnREAL (2015)
  - The Americans (2013)
  - Vikings (2013)
  - Breaking Bad (2008)
  - Chernobyl (2019)
  - Mad Men (2007)
  - The Mandalorian (2019)
  - Berlin Station (2016)
  - Skins (2007)
  - Black Mirror (2011)
- Comedy:
  - Scrubs (2001)
  - Santa Clarita Diet (2017)
  - Norsemen (2020)
  - Im Knast (2015)
  - The IT Crowd (2006)
  - Au serivce de la France (2015)
  - Der Tatortreiniger (2011)
  - Shameless (US) (2011)
  - Fleabag (2016)
  - Jerks (2017)
  - Community (2009)
  - Black-ish (2014)

# Movies
- __TODO__

# Programming Stuff
- Programming Languages
  - Rust (Amazing lifetime- and type-system, too verbose syntax)
  - C (Simple, does the job)
  - Go (Simple Syntax, easy parallelism)
  - Clojure (Great Java-Interop., LISP-like)
  - Haskell (I would like to use it more than i have till now)
  - JavaScript (Yes, i think it is a good language!)
  - python
- Tools
  - zsh
  - (neo)vim
  - Atom
  - LLVM (All of it, not only clang)
  - pandoc (I only discovered it recently)
  - WASM (I like the idea of a typed sandboxed runtime for the web, but some implementation details (e.g. control flow) are weird)

# Random Thoughts on Computer Science
- Why is there no *"pure"* stack machine or stack based bytecode VM? Something with no local variables, only function arguments and the evaluation stack (with instructions picking from it, not only from the top), as discussed [here](http://troubles.md/posts/wasm-is-not-a-stack-machine/). It would be harder to target from compilers, but simpler to be JITed later on.




