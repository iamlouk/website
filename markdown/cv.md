---
title: Curriculum Vitae
author: Lou Knauer
date: 02.07.2020
faicon: fa-address-card
keywords:
  - cv
---

# My CV

You can download my CV [here](/assets/cv.pdf). **It is outdated!**
