---
title: Home
author: Lou Knauer
date: 28.11.2021
faicon: fa-home
keywords:
  - hello
  - world
  - home
description: "Homepage of Lou Knauer, here you can find most of the stuff I worked on over the years."
hidden: True
---

# Welcome to my Website <i class="fa fa-globe"></i>!

**_Work in Progress! <i class="fa fa-hourglass-end" aria-hidden="true"></i>_**

## Some links:

- Github: [\@iamlouk](https://github.com/iamlouk)
- Gitlab: [\@iamlouk](https://gitlab.com/iamlouk)
- My Universities Gitlab: [\@ke17fisi](https://gitlab.cs.fau.de/ke17fisi)
- [Personal Webspace at the University](https://wwwcip.cs.fau.de/~ke17fisi/)
- [Cellular Automata Editor](/projs/simulations/diy-ca/ca-editor.html): Build your own Cellular Automata and find the rules for Game of Life 2.0! (Rust+WebAssembly renderer, JS frontend).
- [Forest Fire Simulator](/projs/simulations/forest.html): Another CA thats fun to look at IHMO.
- [A Continious Infinite-State CA](/assets/vids/cca.webm): Yet another CA, but this time one that was very hard to find good rules for!
- [ASBI: A fun programming language](https://github.com/iamlouk/asbi): _A_ _S_tack _B_ased bytecode-_I_nterpreter with fun syntax: `fib := (i) -> if fibs.i != nil { fibs.i } else { fibs.i = fib(i - 1) + fib(i - 2) };`.
- [A super basic RISC-V emulator](/projs/riscv64-sim/www/index.html): The online-useable version only supports a very small set of instructions, but it is completely dependency-less C that compiles to WebAssembly. On GitHub, I also have a Rust-based RISC-V emulator that supports most of *rv64ic* with a tiny bit of *f*/*d* and *a* (so just enough to make the libc work and be able to use `printf` and so on).
- [Snake](/projs/snake/index.html) and [Withstand](/projs/withstand/index.html).
- [Pythagoras Tree](/projs/config-pythagoras-tree/index.html): A configurable version of [Pythagoras Tree](https://en.wikipedia.org/wiki/Pythagoras_tree_(fractal)).
- [Tables with Colorcodes, ASCII, ...](/tables.html)
- [Sudoku Solver](/projs/solvers/sudoku.html), [Minesweeper + Solver](/projs/solvers/minesweeper.html), [Whatever-this-is](projs/projects/011-jump-n-run/canvas-fun/index.html), ...
- Want to play tetris or snake in the termal? Do `ssh -p 2022 tetris@louknr.net` or `ssh -p 2022 snake@louknr.net`!
- [iamlouk/projects](https://github.com/iamlouk/projects): A repo with a lot of small-ish experiments. Some highlights are:
  - Plugins for [QEMU's TCG](https://github.com/iamlouk/projects/tree/main/017-qemu%2Bdwarf) and [GCC's Middle-End](https://github.com/iamlouk/projects/tree/main/019-gcc-loop-plugin).
  - A shitty (but turing-complete) subset of [Dhall](https://dhall-lang.org/) I called [Rhall](https://github.com/iamlouk/projects/tree/main/025-rhall).
  - A proof-of-concept for a [DSL embedded into python](https://github.com/iamlouk/projects/tree/main/028-python-jit) that JITs functions with a `@jit` decorator.
  - ...

![Image showing a fractal](/assets/pyth-tree.png)

### (Tech-)Blogs I read (sometimes/did in the past)

Here are some (tech) blogs I like to read from time to time. This is kind of a public bookmarks list.

- [Eli Bendersky's website](https://eli.thegreenplace.net/): I really enjoy the format, a blog about a lot of different mostly CS-related topics, but also a few other things like books. I came across it because it's author maintains nice ELF and DWARF parser libs.
- [Drew DeVault's blog](https://drewdevault.com/): A nice blog about a lot of different mostly low-level OS related topics.
- [The Pasture/thephd.dev](https://thephd.dev/): Mostly really fun to read rants about C/C++.
- [Airs - Ian Lance Taylor](https://www.airs.com/blog/): Blog by one of the creators of Go and gccgo, and the gold linker.
- [Julia Evans](https://jvns.ca/): My former favourite blog. Has some extremly nice explanations of how WebAssembly works/fits into the JS/browser ecosystem, but also a lot of diffrent other mostly web-related topics.
- [Developer Voices Podcast](https://www.youtube.com/channel/UC-0fWjosItIOD4ThhS6oyfA): A podcast about different CS stuff.
- [WebKit Dev Blog](https://webkit.org/blog/10308/speculation-in-javascriptcore/): The best blog and explanations on JITs I ever encountered.
- ...


