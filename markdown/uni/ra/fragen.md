---
title: FAU Rechnerarchitektur Prüfungsprotokoll-Fragen (und Antworten)
author:
  - Lou Knauer
highlight-style: pygments
hidden: True
date: 28.01.2021
---

Einige Basics und danach nach Themen sortierte Fragen aus den [Prüfungsprotokollen der FSI](https://fsi.cs.fau.de/dw/pruefungen/hauptstudium/ls3) zur Vorlesung __Rechnerarchitektur__ and der FAU Erlangen-Nürnberg (Stand: Wintersemester 2020/21). Fragen die sich direkt auf die vorherige Frage bezogen haben und/oder nur im Kontext Sinn machen hab ich ab und zu weggelassen.

- Fertig:
  - [2020-02-11](https://fsi.cs.fau.de/dw/pruefungen/hauptstudium/ls3/ra-2020-02-11)
- Schon angefangene Protokolle (ab Abschnitt "Pipelining" fehlt noch):
  - [2020-02-07](https://fsi.cs.fau.de/dw/pruefungen/hauptstudium/ls3/ra-2020-02-07)
- TODO:
  - Viele weitere aus den Jahren davor...!

# TODO:
- Algorithmen nach denen gefragt wurde die ich noch sogut wie gar nicht kann:
  - __Scoreboard-Algorithmus fürs Pipelining__
- Sonstiges:
  - __GPPs__, Signalprozessoren?

# Basics

- CISC: *Complex Instruction Set Computer*
  - Eine Klasse von CPU-Architekturen mit komplexen, unterschiedlich langen Befehlen
  - Beispiele: x86/amd64 (Intel/AMD), z/Architecture (IBM), Motorola 6800, ...
  - Hat Befehle die Speicherzugriffe und arithmetische Operationen mischen
  - Vorteile: Höhere Code-Dichte
  - Nachteile: Macht Compiler komplexer, Pipelining schwerer (nur mit Microcode), CPU Komplexer, ...
- RISC: *Reduced Instruction Set Computer*
  - Eine Klasse von CPU-Architekturen mit wenigen, einheitlich langen Befehlen
  - Beispiele: ARM (ARM, Apple, Samsung, Quallcom, ...), RSIC V (neu, Open Hardware), AVR (Atmel), MIPS, ...
  - Meist mehr Register als CISC, Load-Store-Architektur (Alle Ops. außer Load/Store nur auf Register)
  - Vorteile: Einfachere CPU, besseres Pipelining, Compiler simpler, ...
  - Nachteile: Geringere Code-Dichte
- Pipelining:
  - Jeder Befehl muss sowieso in Unter-Phasen aufgeteilt werden
  - Klassische-Muster-Pipeline:
    1. Befehl holen (Aus Speicher)
    2. Befehl dekodieren (Was bedeutet dieser Befehl, welche CPU-Untereinheit ist gemeint, ...)
    3. Operanden holen (Insb. bei CISC komplex, Operanden laden, ...)
    4. Befehl ausführen (Teilt sich in modernen CPUs selbst nochmal in viele Schritte auf)
    5. Ergebnis zurückschreiben (In Register/Speicher)
  - Idee des Pipelining: In einem Takt führen wir nicht unbedingt eine Instruktion, aber einen dieser Schritte aus. Die einzelnen Schritte überlappen dann aber, sodass mehrere Instruktionen (in unterschiedlichen Stellen) gleichzeitig in der Pipeline sind.
  - Erhöht Durchsatz:
    - Pipeline mit $n$ Instruktionen und $k$ Stufen braucht $k \times (n - 1)$ Takte (Wenn Pipeline durchgefüllt)


# Prüfungsfragen

## CPU Architekturen
- *Aus welchen Gründen ist man von CISC nach RISC gewechselt?*
  - Patterson-Studie: Viele Compiler benutzten eh nicht alle CISC-Befehle
  - Pipelining erfordert "elementare" Befehle (Intel-CPUs bauen CISC zu RISC "on the fly" um, komplex)
  - Code-Dichte mit größeren Hauptspeichern nicht mehr so wichtig
  - "Features" wie Operanden aus Speicher/Register mischbar dem Compiler egal, kein Mensch schreibt mehr von Hand Assembler
- *Wodurch zeichnet sich also RISC aus?*
  - Siehe oben bei [den Basics](#basics)
  - Gleich-lange Instruktionen (32 Bit bei ARM)
  - Speicherzugriffe nur mit Load/Store, alle anderen Instruktionen arbeiten auf Registern
- *Ist CISC also ausgestorben?*
  - Moderne x86-CPUs von Intel/AMD sind CISC-Architekturen die aber aus einer CISC-Inst. zur Laufzeit eine (oder oft mehrere) RISC-Instruktionen machen (Microcode/Microprogrammierung)

## Pipelining
- *Was hat es denn mit dem Pipelining auf sich?*
  - Siehe oben bei [den Basics](#basics)
- *Wieso erhöhen wir eigentlich nicht einfach auf 1000 Stufen?*
  - Für jede Pipeline-Stufe braucht man u.u. eigene (Schatten-)Register
  - Eine einzelne Instruktion wird durch Pipelining nicht schneller
  - Pipeline muss gut gefüllt sein, je länger, desto mehr wirken sich Löcher in der Pipeline aus
- *Sie hatten ja vorhin schon mit dem Speedup angefangen. Was ist da denn der Optimale?*
  - "Bei k Pipelinestufen ist der asymptotische Speedup k." __TODO:__ Quelle?
- *Ist doch schön. Aber wieso klappt das mit dem Pipelining doch nicht ganz so?*
  - Strukturhazards:
    - Gemeinsame Nutzer begrenzter Resourcen, z.B. Speicherzugriffe/Bus/Resource allgemein
  - Datenhazads:
    - RAW: *Read after Write*, WAR: *Write after Read*, WAW: *Write after Write* bei von mehreren Instruktionen Registern/Speicherstellen
  - Steuerungshazards:
    - Bedingte Sprünge, Ausgerechnete Sprungziele
  - Falsch-Vorhergesagte oder berechnete Sprungziele: Pipeline muss geleert werden
    - Funfact: Spekulative Ausführung muss Rückgängig gemacht werden: Meltdown/Specter
- Beispiel für RAW-Datenhazard und was man dagegen tun kann:
  1. `add r1, r2, r3` (`r1 = r2 + r3`)
  2. `add r4, r1, r6` (Problem hier: Operand `r1` kann erst gelesen werden wenn vorheriges Add sein Ergebnis zurückgeschrieben hat!)
  - Was man gegen RAW machen kann: Forwarding: Ergebnis schon vor Writeback-Phase durch Pipeline reichen
- *Wann wissen wir bei Spruengen wo wir hinspringen?*
  - Nach dem Pipelineschritt/der Phase in der die Bedingung ausgewertet bei bedingten Sprüngen
  - Nach dem der Operand geladen wurde bei berechneten Sprungzielen
  - Nach dem der Befehl dekodiert wurde bei unbedingten Sprüngen
- *Wie kann man Steuerungshazards verhindern?*
  - Branch Prediction: Vorhersagen der Sprungziele durch Zustandautomaten/History vorheriger Sprungziele oder sogar durch Perzeptron
  - Zustandsautomat auch "Branch History Buffer" genannt



## Superskalarität
- *Was hat es denn prinzipiell mit der Superskalarität auf sich?*
  - *Instruction Level Paralellism*: Einzelne Instruktionen parallel zu anderen Ausführen
  - CPU hat viele ALUs/Funktionseinheiten (z.B. eine für Integer, eine für Floats, eine für Speicherzugriffe, ..)
  - Ungenutzte Einheiten durch Instruktionen ohne Abhängigkeiten zu schon laufenden ausführen um Durchsatz zu erhöhen.

## Multicore
- *Warum haben wir denn Multicore?*
  - __TODO:__ Da gibt's ne Formel die zeigt das man mit gleich viel Strom mehr Performance durch mehr Kerne als durch höhere Taktung hinkriegt.
- *Wenn wir jetzt einen Skalierungsfaktor von 1/s haben, wie veraendern sich dann die ganzen Groeßen?*
  - __TODO__

## (GP-)GPUs
- *Da gibt's ja dann sowas wie SIMT?*
  - *Single Instruction Multiple Thread*: Mehrere Threads (in einem Warp) führen den selben Befehl aus
  - Eine Variante von SIMD: *Single Instruction Multiple Data*
- *Dann gibt's ja da noch etwas anderes wo nur mit einem Befehl mehrere Daten verarbeitet werden.*
  - SIMD, gibts in "normalen" CPU

## Prozessorarten:
- *Was ist denn der Unterschied zwischen GPPs FPGAs und ASICs?*
  - GPPs: *General Purpose Processor*: Hochperformant, hochentwickelt, flexibel
  - ASICs: *Application-specific integrated circuit*: Anwendungsspezifisch, meist simpler, bei hoher Stückzahl am billigsten
  - FPGAs: *Field Programmable Gate Array*: Teuer, aber flexibel programmierbar/flushbar, HDLs
- *Sind ASICs denn schneller als GPPs?*
  - ASICs vom Design her für ihre jeweiligen Aufgaben besser, aber: GPPs meist mit teureren/besseren Technologieen (höhere Transistordichte etc.) gefertig






