---
title: FAU Rechnerarchitektur WS 2020/21 Zusammenfassung
author:
  - Lou Knauer
highlight-style: pygments
hidden: True
date: 06.02.2021
---

# Kap. 1.: Organisationsaspekte moderner RISC- und CISC-Prozessoren

- CISC:
    - Viele verschiedene Instruktionen mit vielen verschiedenen Speicherzugriff-Möglichkeiten
    - Eine CISC-Instruktion wird durch Microprogrammierung in mehrere RISC-Mikrobefehle
    - Man spricht von programmierbarem Leitwerk (Bei RISC nicht nötig)
- RISC:
    - Alle Befehle haben selbe Länge
    - Speicherzugriffe nur durch explizites Load/Store
    - Elementare, kleine Befehlssätze

## Pipelining

Moderne Preozessoren haben Pipelines mit mehr als 20 Stufen. Simpelste (Lehrbuch-)Variante:

1. Befehl holen
2. Befehl dekodieren (Bei RISC einfacher weil keine Microprogrammierung nötig)
3. Operanden holen (Bei RISC meist einfach weil nur Register)
4. Befehl ausführen
5. Ergebnis zurückschreiben (Bei RISC meist einfach weil nur Register)

Pipelining erhöht nicht die Geschwindigkeit eines einzelnen Befehls (Latenz), sondern den Durchsatz weil die Stufen von verschiedenen Prozessoren überlappen. Pipelining kann einzelnen Befehl sogar länger dauern lassen: Datenbewegung, extra Register pro Stufe, Steueraufwand, etc. Sei $\tau$ die Zeit der längsten Phase einer Pipeline mit $k$ Stufen, dann ist $T_{k} = [k + (n - 1)] \times \tau$ die Gesamtzeit zur Bearbeitung von $n$ Instruktionen. Der erreichbare Speed-Up ist:

$$S_{k} = \frac{T_{0}}{T_{k}} = \frac{n \times k}{k + (n - 1)}$$

## Superskalarität

In einem Prozessor werden mehr als nur eine Instruktion gleichzeitig bearbeitet (Nicht nur wegen Überlappung in der Pipeline, sondern auch auf der gleichen Pipeline-Stufe). Dafür gibt es mehrere Pipelines, mehrere Rechenwerke, etc. Es handelt sich um zur Laufzeit erkannte Mögleichkeiten einzelne Instruktionen parallel abzuarbeiten (-> Umordnung).

Praktisches Beispiel sind z.B. die getrennten Pipelines (Pipeline fächert nach Dekodieren auf) für Ganzzahl- vs. Gleitkommazahl-Arithmetik.

Im Kontext von Superskalarität kann man noch __VLIW__-Architekturen (*Very Long Instruction Word*) erwähnen. Dabei werden in eine Instruktion mehrere parallel ausführbare Aufgaben für verschiedene Recheneinheiten zusammengefasst.

## SMT

SMT, von Intel Hyperthreading genannt, bedeutet einfach nur, dass man auf einem physikalischen Kern zwei Threads gleichzeitig laufen lässt um die Pipeline voller zu kriegen, da Threads unabhängige Kontrollflüsse haben. Dafür braucht man "nur" zusätzliche Registersätze, aber keine zusätzlichen Rechenwerke oder so.

## Hazards

Hazards enstehen druch Pipelining und Superskalarität wenn irgendwo Konflikte zwischen Instruktionen sind.

### Datenhazards

Bei *Read after Write* (__RAW__) soll ein Operand gelesen werden bevor er geschrieben wurde, z.B. weil die vorherige Instruktion die den Operand schreiben würde noch nicht in der Zurückschreiben-Stufe ist wenn der andere Operand geholt/ausgewertet werden soll. Beispiel:
```
r1 := r2 + r3;
r4 := r1 + r5; // Braucht Ergebnis r1
```

Ein *Write after Read* (__WAR__) taucht in einer normalen Pipeline ohne *Out-of-Order-Execution* oder unterschiedlich-lange dauernden Instruktionen nicht auf. Dieser Hazard entsteht wenn eine einen Operanden einer anderen vorherigen Operation überschreibt bevor diese ihn lesen kann. Beispiel:
```
r1 := r2 + r3;
r2 := $5; // Durch Umordnung hätte diese Inst. vor Op. 1 ausgeführt werden sollen
```

Ein *Write after Write* (__WAW__) entsteht u.a. wenn Befehle unterschiedlich lange dauern. Dabei überschreibt eine eigentlich vorherige Operation das Ergebnis einer eigentlich späteren die bereits fertig geworden ist. Beispiel:
```
r1 := r2 / r3; // Braucht lange
r1 := r2 + r3; // Geht schnell
```

Behoben werden solche Hazards indem die Pipeline mit NOPs oder anderen, unabhängigen Instruktionen gefüllt wird. __RAW__ kann man auch durch "Vorreichen" (*Forwarding*) lösen, was moderne CPUs auch wirklich zwischen bestimmten Phasen machen. Man kann diese Probleme auch den Compiler lösen lassen, so in __EPIC__-Architekturen (*Explicitly Parallel Instruction Computing*). Um diese Konflikte in Hardware zu lösen braucht es Scoreboarding-Algorithmen:

### Scoreboard-Algorithmus

Das Scoreboard bescheibt hier eine Tabelle mit dem Zustand aller Recheneinheiten (z.B. *Load/Store*, *Integer-Add/Mul*, *Integer-Div*, *Float-Add/Mul*, ...), der jeweils verwendeten Register, und ob diese schon Verfügbar sind. Diese Tabelle wird immer wieder aktuallisiert und anhand dieser wird entschieden wann eine Instruktion eingelastet wird. Der klassische, simple Scoreboard-Algo. unterteilt die Dekodier- und Rückschreibe Phase nochmal weiter:

1. Befehl holen
2. Befehl dekodieren
    - Warten auf freie Funktionseinheit (Strukturhazards lösen)
    - Warten bis niemand anderes das Zielregister schreiben will (__WAW__ vermeiden)
3. Operanden holen
    - Warten bis Operanden verfügbar (__RAW__ vermeiden)
4. Befehl ausführen
5. Ergebnis zurückschreiben
    - Vorher warten bis alle anderen Instruktionen das Zielregister gelesen haben (__WAR__ vermeiden)

Einträge in der Scoreboard-Zustandstabelle, die immer wieder aktualisiert wird, zu jeder Funktionseinheit:

- *Busy* (Boolean): Ob Funktionseinheit frei ist
- *OP*: Auszuführende Operation
- $F_{i}$: Zielregister
- $F_{j}$, $F_{k}$: Quellregister
- $Q_{j}$, $Q_{k}$: Funktionseinheiten, die die Quellregister schreiben (werden)
- $R_{j}$, $R_{k}$: Flags die Angeben ob $F_{j}$, $F_{k}$ (schon) nutzbar sind

In der Vorlesung wird von vier Stufen gesprochen (Nach jedem Zustandswechsel einer Instruktion wird das Scoreboard aktuallisiert):

1. Instruktion dekodieren und auf Funktionseinheit warten
2. Operanden lesen (ggf. warten bis Operanden verfügbar)
3. Operation ausführen
4. Ergebnis zurückschreiben (vorher warten bis Zielregister geschrieben werden kann)

### Tomasulo-Algorithmus

Im [Tomasulo-Algo.](https://en.wikipedia.org/wiki/Tomasulo_algorithm) wird zusätzlich zum klassischen Scoreboarding noch Registerumbenennung (bzw. Zeiger auf andere Reserverungsstation) betrieben um die Pipeline voller zu kriegen aber trotzdem Konflikten zu entgehen. Eine Reserverungsstation speichert zusätzlich zu einer Funktionseinheit noch Daten (zwischen). Zusätzlich zum Scoreboard wird sich zu jedem Register gemerkt welche Funktionseinheit darauf schreibt. Der große Vorteil dieses Algorithmus ist, dass nicht auf das Zielregister gewartet werden muss bevor eine Instruktion losrechnen kann.

Es gibt drei Phasen:

1. Befehl installieren
    - Dispatch: Reserverungsstation belegen (Strukturhazards lösen)
    - Issue: Operanden holen (von einem Bus zwischen allen Reserverungsstation mit Registerumbenennung)
2. Ausführung
    - Falls Operanden noch nicht nutzbar beobachte Datenbus
3. Ergebnis Zurückschreiben
    - Über *Common Data Bus*
    - Reserverungsstation freigeben

Performance kann durch *Instruction-Reorder-Buffer* (__ROB__) bzw. Instruktionsumordnungen verbessert werden. Dabei wird zwar *In-Order* committed (also in den Speicher oder ein "richtiges" Register geschrieben), es kann aber *Out-of-Order* executed werden. Der [ROB](https://en.wikipedia.org/wiki/Re-order_buffer) speichert alle Werte die in der Zurückschreiben-Phase entstehen, und committed sie dann so wie sie wirklich hätten passieren sollen. Außerdem ermöglicht der ROB ein Rollback, welches z.B. wegen falscher Sprungvorhersage oder einem synchronen Interrupt nötig sein kann (Quasi ein Undo, nur wurden hier sachen wie Cache-Effekte vergessen und jetzt haben wir [Meltdown/Spectre](https://meltdownattack.com/)).

### Steuerungshazards

Steuerungshazards (Auch Kontroll-Hazard) entstehen durch Sprünge. Bei einem bedingten Sprung kann es vorkommen dass die als nächstes abzuarbeitende Instruktion nicht die als nächstes im Speicher liegende ist. Die Pipeline muss geflusht/angehalten werden. Werden durch Sprungvorhersage minimiert, oder ihr Effekt durch spekulative Ausführung ausgeglichen (Dann müssen Operationen aber ggf. wieder Rückgänig gemacht werden).

### Struktur-Hazards

Enstehen eher bei superskalaren und/oder SMT-Prozessoren. Dabei wird eine Resource (z.B. eine ALU, ein Bus, etc.) gleichzeitig von verschiedenen Instruktionen gebraucht. Kann/Muss durch Warten behoben werden.

## Sprungvorhersage

Statische Sprungvorhersage macht man nicht mehr wirklich, aber früher z.B. anhand des Opcodes.

Dynamische Sprungvorhersage verwendet *Branch loop buffer* (wahrscheinlich aufeinanderfolgende Befehle buffern), *Branch prediction buffer* (1-Bit-Historie vorheriger Sprünge) und *Branch history table*'s, die sich merken wohin an einer bestimmten Stelle im Programm zuletzt gesprungen wurde und dann mit diesen Informationen und einem Zustandautomaten (Oder in fancy und neu sogar einem Perceptron) entscheidet wohin dieses mal wahrscheinlich gesprungen wird.

Beim *delayed branch* (__verzögerter Sprung__) wird die Auswerung der Bedingung des Sprungs vorgezogen damit dessen Ergebnis schon da ist wenn die Pipeline an der Stelle des Sprungs ist und entschieden werden muss. Funktioniert nur wenn Sprungbedingung nicht erst kurz vor Sprung ermittelt wird. Praktische Umsetzung kann sein: Sprungziel schon während der Dekodier-Phase bestimmen (Dann nur eine einzige NOP in der Pipeline nötig).

## Speicher-Organisation

+--------------------+--------------+-------------+
| Speicherhierarchie | Zugriffszeit | Kapazität   |
+====================+==============+=============+
| Register           | 1 Takt       | ~512 Bytes  |
+--------------------+--------------+-------------+
| L1 Cache           | 1-3 Takte    | ~4 Kb       |
+--------------------+--------------+-------------+
| L2 Cache           | 1-6 Takte    | ~0.5 Mb     |
+--------------------+--------------+-------------+
| L3 Cache           |              |             |
+--------------------+--------------+-------------+
| [L4 Cache]         |              |             |
+--------------------+--------------+-------------+
| Hauptspeicher      | ~30 Takte    | ~16 GB      |
+--------------------+--------------+-------------+
| Festplattencache   |              |             |
+--------------------+--------------+-------------+
| Festplatte         | ~5 ms        | ~500 GB     |
+--------------------+--------------+-------------+
| ...                |              |             |
+--------------------+--------------+-------------+

Mittlere (effektive) Speicherzugriffszeit ($m$ = Fehlerrate, $T_{m}$ = Nachladezeit/Miss, $T_{h}$ = Cache-Zugriffszeit/Hit):

$$T_{a} = T_{h} + m * T_{m}$$

Caches sind in Blöcken und Zeilen organisiert. Daten werden min. Zeilenweise in einen Cache reingeladen, meisten ca. 64 Bytes. Nur zwischen Registern und L1-Cache wird meistens noch Wort/Byte-weise geladen. Caches funktionieren nur wegen zeitlicher (Man greift oft mehrmals auf die selbe Speicherstelle hintereinander zu) und räumlicher (Man greift oft auf zusammenhängenden Speicher zu, z.B. Array-Einträge) Lokalität.

Der __L1-Cache__ ist oft aufgeteilt in einen Instruktions- und einen Datencache, um danach besser dekodieren zu können. Außerdem ist dieser quasi immer virtuell Addressiert, wohingegen die Tags (und damit deren Addressierung) in den höheren Caches physikalische Addressen sind.

### Cache-Assoziativität

Assoziative Cache besteht aus mehreren Mengen, innerhalb einer Menge wird anhand des Tags der richtige Block ausgewählt. Die Menge an sich wird durch einen Index-Anteil der Addresse ausgewählt (z.B. `set_idx = (addr & 0x000000f0) >> 4` wenn es 4 verschiedene Mengen gibt). Extremfälle sind vollassoziative Caches (es gibt nur eine große Menge, keinen Index) und direktabbildende Caches (Es müssen keine Mengen geführt werden, Block wird nur über Index-Bits gefunden).

Größen müssen Abgewogen werden:

- Höhere Assoziativität verschlechtert $T_{h}$
- Erhöhung der Blockgröße verschlechtert $T_{m}$
- Gibt spezielle Maßnahmen wie Victim-Caches in seltenen Architekturen

### Cache-Aktualisierung/Ersetzungsstrategien

Bei assoziativen Caches:

- __LRU__: Rausschmeißen was als letztes benutzt wurde
- __LFU__: Rausschmeißen was am seltensten benutzt wurde
- __FIFO__: Rausschmeißen was als erstes reingepackt wurde
- pseudo-random: Funktioniert besser als man so denkt, simpel.

Klassifikation von Fehlzugriffen:

- *Compulsory*: Kaltstart-Miss, Block muss das erste mal gelesen werden
- *Capacity*: Block war mal da aber wurde zwischenzeitlich wegen Platzproblemen verdrängt
- *Conflict*: Kollision bei den Index-Bits mit Block mit anderem Tag

### Cache-Kohärenz

Cache-Kohärenz beschreibt das Phänomen, dass von Prozessor 1 in dessen L1-Cache geschriebene Daten je nach Architektur (bei x86/x64 z.B.) von jedem anderem Prozessor auch so gelesen werden können muss.

__Snoopy-Protokolle__ setzen auf dezentralle Kontrolle auf alle lokalen Cache-Controller. Dabei wird bei schreiben einer Zeile dies per Broadcast im Chip verbreitet. Dies erhöht den Datenverkehr auf dem Bus und zwischen den Prozessoren. Bei *Write-Invalidate* darf nur ein Kern schreiben, in allen anderen Caches wird die Zeile "beansprucht" und invalidiert. Bei *Write-update* wird nach jedem Schreiben sofort der neue Wert an alle anderen Caches weitergeleitet.

Das __MESI-Protrokoll__ ist ein *Write-Invalidate*-Verfahren bei dem jede Cache-Zeile einen der folgenden Zustände haben kann:

- __M__ (*Modified*): Datum nur in diesem Cache und verändert (gegenüber dem Hauptspeicher)
- __E__ (*Exclusive*): Datum nur in diesem Cache und nicht verändert
- __S__ (*Shared*): Datum ev. in anderen Caches und nicht verändert
- __I__ (*Invalidated*): Dieser Cache-Eintrag ist ungültig, muss neu geladen werden

### Cache-Effekte

#### Array Merging

```c

/* Bad: */
int a[10000];
int b[10000];

for (int i = 0; i < 10000; i++)
    do_stuff(a[i], b[i]);

/* Good: */
// Bessere Lokalität der Speicherzugriffe!
struct pair {
    int a;
    int b;
} data;

for (int i = 0; i < 10000; i++)
    do_stuff(pair[i].a, pair[i].b);

```

#### Schleifen-Vertauschung

```c
int A[1000][1000];

/* Bad: */
for (int j = 0; j < 1000; j++)
    for (int i = 0; i < 1000; i++)
        do_stuff(A[i][j]);

/* Good: */
// "Kontinuierlicher" Speicherzugriff statt
// größe Lücken zwischen Zugriffen!
for (int i = 0; i < 1000; i++)
    for (int j = 0; j < 1000; j++)
        do_stuff(A[i][j]);

```

#### Schleifen-Fusion

```c
int a[1000];
int b[1000];
int c[1000];

/* Bad: */
for (int i = 0; i < 1000; i++)
    c[i] += a[i];

for (int i = 0; i < 1000; i++)
    c[i] += b[i];

/* Good: */
// `c[i]` muss nicht 2x geladen werden
for (int i = 0; i < 1000; i++)
    c[i] += a[i] + b[i];

```

#### Blocking

```c
int A[1000][1000];
int B[1000][1000];

/* Bad: */
for (int i = 0; i < 1000; i++) {
    for (int j = 0; j < 1000; j++) {
        B[i][j] = (
            A[i - 1][j] +
            A[i + 1][j] +
            A[i][j - 1] +
            A[i][j + 1]) * 0.25;
    }
}

/* Good: */
// Vorher verwendete/geladene A-Elemente werden
// bei klein-genugem Block nicht aus dem Cache
// verdrängt bevor sie wieder verwendet werden.
for (int bi = 0; bi < 1000; bi += 100) {
    for (int bj = 0; bj < 1000; bj += 100) {
        for (int i = bi; i < bi + 100; i++) {
            for (int j = bj; j < bj + 100; j++) {
                B[i][j] = (
                    A[i - 1][j] +
                    A[i + 1][j] +
                    A[i][j - 1] +
                    A[i][j + 1]) * 0.25;
            }
        }
    }
}

```

# Kap. 2.: Homogene und Heterogene Multi-/Vielkernprozessoren

[Moore's Law](https://en.wikipedia.org/wiki/Moore%27s_law): *"The number of transistors in a dense integrated circuit doubles about every two years"*

Das wurde allgemein als auch eine verdoppelung der Leistung verstanden.

- Höhere Transitordichte -> Komplexere Architekturen
- Höhere Transistorleistung -> Höhere Frequenz
- Geringerer Energieverbrauch beim Schalten -> Gesamt-Leistungsdissipation bleibt handhabbar

Leistungssteigerung so aber nicht mehr alleine erreichbar (z.B. wegen physikalischer Grenzen beim Stromverbrauch und der Transistorgröße), deswegen: Anstatt Takt erhöhen lieber auf mehr parallele Kerne setzen. Für CMOS-IC-Tech gilt:

$$\text{Power} = \text{Capacitive load} \times \text{Voltage}^{2} \times Frequency$$

$$P_{diss} = C \times \rho \times f \times V_{dd}^{2}$$

Die Frequenz ($f$) erhöhen bedeutet also, dass die Spannung quadratisch erhöht werden muss. Wenn man dagegen einen zweiten Prozessorkern dazupackt steigt Kühlbedarf nur linear. Außerdem kann mehr als ein Kern pro DIE billiger zu Produzieren sein.

- Multikern-Prozessoren: 2+ Kerne, Intel x86, AMD x86, Sparc, IBM Power, ...
- Vielkern-Prozessoren: 100+ Kerne, GPUs, Intel Polaris, Xeon Phi, ...
- Man unterscheidet Homogene (Bisher die meisten Desktop-Multikern-Prozessoren) und Heterogene Prozessoren (Oft in Handys, [ARM big.LITTLE](https://en.wikipedia.org/wiki/ARM_big.LITTLE), (GP-)GPUs, Cell)

Pollacks Regel: Einzelnen Kern doppelt so groß/komplex machen -> 40% mehr Leistung, Anstieg Rechenleistung ~ $\sqrt{\text{Anstieg Komplexität}}$.

Vorteile von Multikern-Prozessoren:

- Leistung verdoppelt sich bei doppelter Anzahl an Kernen
- Kühlbedarf/Spannung steigt linear mit zusätzlichen Kernen
- Einzelne Kerne ab-/anschaltbar
- Zuverlässigkeit und Leckströme geringer bei niedriger Temperatur

## Reicht Multikern?

__Amdahlsches Gesetz__: Speed-Up eines Programmes immer limitiert durch den seriellen Anteil ($n$ = #Kerne, $s$ = Serieller Anteil in %):

$$S_{par} = \frac{1}{s + \frac{1 - s}{n}}$$

Viele Anwendungen sind allerdings leicht und gut parallelisierbar, z.B. DL oder wissenschaftliches Rechnen.

Ein anderes Problem ist, dass Kerne __speicherhungrig__ sind, und die Speicherbandbreite oft limitiert. Hierzu gibt es Ideen wie 3D-Stapeltechniken oder das verteilen des Speichers auf viele Bänke (wie z.B. bei GPUs, erhöht aber die Latenz).

## Roofline-Modell

Das [Roofline-Modell](https://en.wikipedia.org/wiki/Roofline_model) verwendet man gerne zu für eine bestimmte Anwendung und Architektur entscheiden zu können wie/ob man noch weiter Optimieren kann. Dabei wird auf der Y-Achse die Performanz und auf der X-Achse die Operationelle Intensität aufgetragen. Jede Architektur hat eine gwisse maximale Performanz, diese limitiert die erreichbare Leistung nach oben als Patellele zur X-Achse. Die Erreichbare Speicherbandbreite hängt von der Operationellen Intensität einer Anwendung und limitiert die maximale Performanz als linear ansteigende Gerade.

Die Operationelle Intensität hat die Einheit (im Fall von `float`-berechnenden Anwendungen) $\frac{\text{FLOPS}}{\text{byte}}$, also die Anzahl an Operationen pro Bytes die man dafür Laden muss.

$$P_{attainable} = min(P_{peak}, \text{Memory-Bandwith} \times \text{OpIntens.})$$

Wenn Speicherbandbreite limitiert, dann:

- Caches besser nutzen
- Speicherzugriffe auf mehrer Speicherbänke aufteilen
- ...

Wenn die Rechenleistung limitiert, dann:

- SIMD nutzen
- Loop-Unrolling
- ...

## Beispiele: Entwicklungen bei Intel von Nehalem bis Skylake

- Tick-Tock-Prinzip (Nicht mehr, war mal): Abwechselnd neue Mikroarchitektur oder neue Prozess-Technologie
- Modularer Aufbau mit wenigen Grundbausteinen, z.B. *Quick Path Interconnect*, *Integrated Memory Controller*, Kerne mit eigenen L1- und L2-Caches, L3-Cache, GPU-Kern, ...
- *Loop Stream Detector* speichert jetzt Mikrooperationen statt Makro-Instruktionen um Dekodieraufwand zu minimieren
- Spannung und Frequenz einzelner Kerne einzeln Regelbar: *Turbo-Boost*
- Hyperthreading
- Speichercontroller in die CPU integriert
- MESIF-Prtokoll zur Cache-Koheränz
- Mikrobefehls-Cache (nicht nur für Schleifen)
- Ein großer Registersatz mit *Zeigern* (so in der Art) bei Registerumbenennung statt kopieren
- ...

## Beispiele: Zen-Architektur von AMD

- Perceptron für Sprungvorhersage
- Caches für Microinstruktionen
- Dekoder holt 4 Instruktionen pro Zyklus
- Getrennter Instruktions-Scheduler für *Int*/*Float*-Einheiten
- L1-Cache *write back* statt *write through*
- Höhere Assoziativität in den Caches

## GPGPUs

GPUs hatten ursprünglich keine einheitliche Programmierschnittstelle und hatten nur *Fixed-Function-Pipelines* für spezifische Anwendungen. Shader-Programmierung war der Anfang der GPGPUs (heutzutage auch Turing-Vollständig).

Aufbau ([hier](https://images.idgesg.net/images/article/2018/09/lkdfghpzhx-17-100771740-orig.jpg) als Bild am Beispiel einer Nvidia-Karte):

- Bus-Interface (PCIe, NV-Link, ...)
- (GPU-)Globaler Speicher (Besteht aus vielen Bänken für hohe Bandbreite)
- Scheduler (*Gigathread Scheduler Engine* bei Nvidia)
- Multi-Prozessoren (z.B. so 13 bei der `k20m`)
    - Lokaler Speicher (schneller als der globale Speicher, nur für Threads im selben Block)
    - Wenige *Special Function Units* (Für sowas wie e-Funktion, Trigonometrie, ...)
    - (Instruktions-, Konstanten-, Texturen-)Caches
    - *Multithreaded Instruction Unit* (So nennt Nvidia das zumindest)
    - Viele Streaming Prozessoren (z.B. so 64 bei einigen Nvidia-Karten)
        - Sehr simpel Aufgebaut, ein bisschen wie eine fette gemeinsame SIMD-Einheit
        - Alle Threads führen selbe Instruktion aus

Programmiermodell:

- *SIMT*: *Single Instruction Multiple Thread*
- 1D/2D/3D-Feld-Aufteilung für Datenparallele Aufgaben
- Grid aus Blöcken aus Threads (Threads in den selben Blöcken teilen sich lokalen Speicher etc.)
- Kernel werden vom Host aufgerufen und laufen auf CPU: `my_kernel<<<gridDim, blockDim>>>(...);` (*Function Offloading*)
- Speicher muss explizit zwischen Host/Device transferiert werden (`cudaMemcpy`)

Wenn ein Thread einen anderen Kontrollfluss nimmt als die anderen gerade im selben Block/Multi-Prozessoren laufenden Threads, dann wird der aktuelle *Warp* (Gruppe gleichzeitig auf gleichem MP laufender Threads) aufgeteilt und der eine Teil muss warten und wird später eingelastet.

GPUs machen komische/coole Sachen mit Speicherzugriffen. Zum einen wird ein Warp pausiert wenn er einen Speicherzugriff macht der dauert und durch einen anderen ersetzt der hoffentlich sofort weiterlaufen kann um Latenzen zu verstecken und den Durchsatz hoch zu halten. Die bedeutet aber das man immer mehr Blöcke als MPs haben sollte. Außerdem betreiben GPUs __*Coalescing*__: Die Speicherzugriffe eines Warps werden in Transaktionen zusammengefasst, das funktioniert zwar nur gut bei gutem Alignment, vermindert aber den Kommunikationsaufwand etc.

## Sonstiges

Die __HSA__ (*Heterogeneous Systems Architecture*) unterscheidet zwei Kern-Typen: *Logical Compute Units* (quasi CPUs) und *Throughput Compute Units* (quasi GPUs). Für TCUs gibts eine eigene Zwischensparache (*HSAIL*) die auf RSIC V bassiert und den seriellen ablauf eines einzelnen Threads beschreibt. HSAIL wird dann später ge-JIT-et.

GPUs haben bisher meist einen eigenen, anders addressierten Speicher. Koheränter, ebenfalls virtueller Speicher wäre doch cool!

# Kap. 3.: Eingebettete Prozessoren / Spezialprozessoren

Man unterscheidet:

- Universalprozessoren: GPPs (*General Purpose Processors*): RISC/CISC, ...
- Mikrocontroller, DSPs (Digitale Signalprozessoren), ASIPs (*Application Specific Instruction Set Processor*)
- Programmierbare Hardware: FPGAs (*Field Programmable Gate Array*), CPLD (*Complex Programmable Logic Device*)
- ASICs (*Application Specific Integrated Circuit*)

Diese Liste ist absteigend sortiert nach Flexibilität, Leistungsverbrauch und Kosten, und aufsteigend sortiert nach Leistung (in bestimmten Domänen) und Stückzahl (~grob, FPGAs gibts in der Wildnis glaube ich nicht ganz so viele).

## Eingebettete Systeme

> „... ein Computersystem, was in ein technisches System eingebettet ist, selbst aber nicht als Computer erscheint“ [R. Ernst]

Eingebettete Systeme sind oft als digitale Systeme mit analoger Umwelt in Kontakt und haben Aktoren/Sensoren. Sie werden speziell für bestimmte Anwendungen entworfen.

### Mikrokontroller

- Steuerungsdominante Anwendungen
    - geringer Datendurchsatz
    - Multitasking
    - auch geringe Wortbreiten ausreichend
- Register oft als RAM realisiert
- Periphere Einheiten integiert: A/D-Wandler, CAN-Bus-Zeugs, Timer, Interrupt-Controller, ...
- Hochleistuns-Mikrocontroller:
    - Beispiele: Infineon Tricore / Aurix, Intel x186, Motrorola, MIPS, ARM Cortex-A), ...
    - Einsatzgebiete in Telekommunikation, Automobiltechnik, ...

### Digitale Signalprozessoren (DSPs)

- Anwendung mathematischer Operationen auf digital repräsentierte Signale
    - Abtasten/Sampeln, A/D- und D/A-Wandler, ...
- Einsatzgebiete (Auswahl):
    - Telefone, VoIP, Audioproduktion, Musiksynthese, Modems, Rauschunterdrückung
    - GPS, Satellitenkomunikation, Verschlüsselung von Kommunikation
    - Radar, Med. Unltraschall, Motosteuerung
    - ...
- GPPs können heutzutage einiges davon auch selbst (insb. (De-)Codierung, ...)

### FPGA

- Wird programmiert, arbeitet Programm aber nicht ab sondern ändert interne Konfiguration entsprechend
- Programmierbar durch sowas wie System-C oder VHDL
- Nach (neu-)konfiguration wie ein ASIC
- Häufig für Prototyping

### ASIC (*Application Specific Integrated Circuit*)

- Löst nur ein bestimmtes Problem
- Schneller, kostengünstiger (bei hoher Stückzahl) und stromsparrender als GPP
- Hohe Fixkosten

## Zusammenfassung Aufgaben Autonomes Fahren

- Sensing
    - GPS
    - LiDAR
    - Cameras
- Perception
    - Localization
    - Object recognition
    - Object tracking
- Decision-making
    - Prediction
    - Path planning
    - Obstackle avoidance

## Eingebettete KI

Eingebettete KI's oft nur für Inferenz, nicht für Training. Man unterscheidet bei KI-Hardware-Architekturen zwei Kategorien:

- Temporale Architekturen (*SIMD*/*SIMT*)
    - CPUs/GPUs
    - Eine zentrale Kontrolleinheit für viele ALUs
    - ALUs holen Daten über Speicherhierarchie, nicht direkt voneinander
- Spatiale Architekturen (*Dataflow Processing*)
    - Häufig in ASICs und FPGAs
    - Beliebt für DNNs
    - ALU hat eigenen lokalen Speicher
    - "ALUs bilden die Prozessoreinheit" (*Was genau soll das denn heißen?*)

__Systolische Felder__ sind irgendwie eine Kombination aus beidem und beliebt in der digitalen Signalverarbeitung. Es ist ein synchron getaktetes System aus vielen, hintereinander-gepackten, die selbe Operation ausführenden Prozessorknoten (Ausgabe des einen Knoten ist Input des nächsten, wie bei spatialen Architekturen). Anwendungsbeispiel: Convolution (dt. Faltung) in CNNs. In jedem Takt wird ein neues $a$ bzw. $b$ "nachgepumpt", also $j$ erhöht. Beispiel (Ein Prozessorknoten rechnet $y = a \times b + x$):

$$y = 0 + (a_{(i, j)} \times b_{(i, j)}) + (a_{(i+1, j)} \times b_{(i+1, j)}) + (a_{(i+2, j)} \times b_{(i+2, j)}) + ...$$
