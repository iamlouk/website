---
title: "FAU: Deep Learning 2020 Comprehensive Questions"
author: Lou Knauer
date: 20.07.2020
hidden: True
---

# FAU: Deep Learning 2020 Comprehensive Questions

## 0.: Introduction:

- What are the six postulates of pattern recognition?
  1. Availability of a representative sample of patterns for the given field of problems
  2. A pattern has features which characterize its membership in a certain class
  3. There are compact domains of features of the same class, domains of different classes are separable
  4. A pattern consists of/can be decomposed into simpler constituents, which have certain relations to each other
  5. A pattern has a certain structure, not any arrangement of constituents is a valid pattern
  6. Two patterns are similar if their features or simpler constituents differ only slightly
- What is the Perceptron objective function?
  - Minimiere in Bezug auf die weights: $D(w) = -\sum_{x_i \in Missclassified} y_i * (w^T * x_i)$
- Can you name three applications successfully tackled by deep learning?
  - Klassische FFNs: Klassifizieren von Daten bei dennen Features bereits extrahiert wurden, z.B. Preisschätzungen
  - CNNs: Bilderkennung, Feature Extraction bei Bildern
  - Reinforcement Learning: Spiele
  - GANs: Neue Bilder/pseudo-Kunst/... generieren

## 1.: Feed-Forward Neural Networks

- Name a loss function for multi-class classification in deep learning.
  - Softmax Activation Function
    - $\forall_{y_i \in Y}: 0 <= y <= 1$
    - $\sum_{y_i \in Y} y_i = 1$
    - Da kommt also eine Wahrscheinlichkeitsverteilung/Histogramm raus
    - Passt sehr gut zu Hot-1-Encoding
  - CrossEntropyLoss: $L(y, ŷ) = -log(ŷ_k)|_{y_k = 1}$
- Explain how this loss function works.
  - CrossEntropyLoss paired with Softmax in last layer
  - "Naturally handles multiple class problems"
- How can you check if the derivative implementation of a loss function is correct?
  - Compare analytical with finite differences results
- What does backpropagation do?
  - Recursively apply chain rule and calculate gradients with respect to everything we need. See [Page 30](./Lecture Slides/1_NeuralNetworks_handout.pdf)
- How does backpropagation work?
  - $ŷ = f(x_1, x_2)$
  - From prev. Layer: $\frac{\partial L}{\partial ŷ}$
  - Grad. for $x_1$: $\frac{\partial L}{\partial x_1} = \frac{\partial L}{\partial ŷ} \frac{\partial ŷ}{\partial x_1}$
  - Grad. for $x_2$: $\frac{\partial L}{\partial x_2} = \frac{\partial L}{\partial ŷ} \frac{\partial ŷ}{\partial x_2}$
  - Grad. $\frac{\partial ŷ}{\partial x_n}$ can be computed only with knowledge from this layer
- Explain the exploding and vanishing gradient problems.
  - We calculate the product of partials in the backpropagation: Gradient might vanish or explode over layers
  - Activation Functions with constant parts (`sign`, `ReLU`) or saturations (`tanh`, `sigmoid`): gradients vanish
- Why is the signum function not used in deep learning?
  - Gradient vanishes everywhere but is infinity for zero
  - No "normal" derivate, cannot learn/backpropagate

## 2.: Loss and Optimisation

- What are our standard loss functions for classification and regression?
  - Classification: Negative Log Likelihood/Cross Entropy ($-log(ŷ)$)
  - Regression: $L^{1}$/$L^{2}$-Loss
- What assumptions do our standard loss functions imply?
  - Outputs of networks are probabilities
  - we need labeled data
  - labels are equally distributed
- What is a subdifferential at a point x0?
  - All vectors $g$ for which: $f(x) >= f(x_0) + g^T @ (x - x_0)$
- How can we optimize a non-smooth convex function?
  - classical gradient descent is impossible
  - Use subgradient algorithm: follow those, not the actual gradient
- What if somebody tells you, to use an SVM because it is superior?
  - Both are equivalent "up to an overall multiplicative constant"
  - Outliers are punished linearly (*TODO: Only for SVM? This could be true for other cases too?*)
  - SVMs can be integrated into a neural network, even for multi-class problems
- What is Nesterov Momentum?
  - Momentum: Add previous gradients to the current mini-batch gradient
  - classically: $v^(k) = beta1 * v^(k-1) - lr * \Delta L(w^(k)); w^(k+1) = w^(k) + v^(k)$
  - "compute the gradient in the direction we’re going anyway": $v^(k) = beta1 * v^(k-1) - lr * \Delta L(w^(k) + beta1 * v^(k-1)); ...$
- Describe Adam.
  - "Adaptive Moment Esitimation": Based on Idea of individual learning rates
  - Calculate $(\Delta L(w^(k)))**2$ (element wise)

## 3.: Activation Functions and Convolutional Networks

- Name five activation functions/Discuss those 5 activation functions.
  1. Linear: $f(x) = a*x$
    - Does not introduce non-linearity: We cannot go deeper
    - opt. problem convex
  2. Signum: $f(x) = -1 if x < 0 else 1$
    - zero-centered
    - cannot train because of missing gradient
  3. Sigmoid: $f(x) = 1 / (1 + exp(-x))$
    - Output in [0; 1]
    - saturates: vanishing gradient
    - not zero-centered
  4. Tanh: $f(x) = tanh(x)$
    - zero-centered
    - saturates: vanishing gradient
  5. ReLU: $f(x) = max(0, x)$
    - no vanishing gradient
    - good generalization due to pice-wise linearity (*TODO: What?*)
    - not zero-centered
    - dying ReLU: weight/biases trained to always yield negative x: gradient zero
  6. Others: LReLU, ELU, SELU, ...
- What is the zero-centering problem?
  - Output of many activation functions shifted towards positive numbers only
  - Later layers have to learn to handle this (But: BatchNorm can help)
- Why does ReLU as activation function perform much better than sigmoid/tanh in a large number of tasks?
  - See above
- Why are convolutional networks well suited for image and audio processing?
  - "Pixels are **bad** features!": Extract features from image by passing "filters" over it
  - Other features composed of previously discovered patterns
- Write down a mathematical description of strided convolution.
  - Stride is like subsampling
  - *TODO: What?*
- What is the connection between 1 × 1 convolutions and fully connected layers?
  - When kernel-size is same as width/height as input, both layers are fully equivalent
- How would you implement a classifier which operates on image patches?
  - convolutional neural networks!
- What is a pooling layer?
  - max./avg. pooling: combine multiple pixels into one
- Why do we use pooling layers?
  - decrease number of parameters/computational cost
  - reduce overfitting
- On what data would CNNs probably perform bad?
  - no connection between near by features
  - no "feature extraction" needed

## 4.: Regularization

- What is the bias-variance tradeoff?
  - "we'd like to minimize bias and variance", but impossible in general
  - model capacity describes how many functions model can approximate
  - higher model capacity -> reduce bias
- What is model capacity?
  - See above
- Describe three techniques to address overfitting in a neural network.
  1. Data Augmentation: Artificially enlarge dataset
  2. L1/L2 Regularization, weight decay: enforce small norm increase bias, reduce variance
  3. Batch Normalization: substract mean, normalize variance
  4. Dropout: Randomly set activations to zero (force model to take new paths)
  5. make model learn a related task as well (similar to transfer learning)
- Why do we often need a validation set?
  - check for overfitting
  - estimate how model will perform on real/new data
- Can we optimize the hyperparameter λ by gradient descend on the training set?
  - Learning a hyperparameter takes a very long time
- What connects the covariate shift problem and the ReLU?
  - ReLU is not zero-centered, output of ReLU only positive
- How can we address the covariate shift problem?
  - BatchNom layers
- Which problem do current initialization schemes try to address?
  - DL problems are not convex: initialization matters
  - weights need to be random to break symmetry, they influence stability
- What is transfer learning?
  - reuse models trained on a similar task, e.g. for small medical datasets
  - different task on same data, different data for same task, different data for different task

## 5.: Common Practices

*No comprehensive questions*

## 6.: Architectures

- What are the advantages of deeper models in comparison to shallow networks?
  - exponential feature reuse
  - increasingly abstract features
  - more layers -> more parameters -> higher model capacity
- Why can we say that residual networks learn an ensemble of shallow networks?
  - residual mapping: $H(x) = F(x) - x <=> F(x) = H(x) + x$, skip connections
  - "Implicitly average exponentially many networks"
  - $x_{i+1} = x_i + H_{i+1}(x_i)$
- How does a bottleneck layer work?
  - 1x1 filters: remove redundancy in correlated features
  - reduce number of features to be convolved by first going down and than back up with the number of channels
- What is the standard inception module and how can it be improved?
  - Idea from network in networks: split and rejoin, different paths have different filter sizes etc.
  - Improvements: Replace 7x7/5x5 convolutions by multiple 3x3 convolutions

## 7.: Recurrent Neural Networks

- What is the strength of RNNs compared to feed-forward networks?
  - they have hidden/cell states in which temporal context can be stored
  - usage for: speech, music, video, sensor series, ...
  - alternative would be to feed hole sequence to FNN at once:
    - hard to train, difference between spatial and temporal dimensions
    - inefficient memory usage
    - not real-time
- What role does the hidden state play in RNNs?
  - at $t_{i}$ additionally to $x_{t}$ we also get $h_{t-1}$ as input
  - hidden state $h$ passed from previous time step to the current one
- How do you train RNNs? What are the challenges?
  - "unfold" RNN through the time dimension, imagine loop over time
  - *Backpropagation through time*: start with last unfolded RNN cell and backpropagate to the past
  - we need full sequence during training
  - loss is sum of all losses at every time step
  - improvement: *Truncated Backpropagation through Time*
    - run overlapping backpropagations on parts of the sequence
    - loss from only some range of unfolded cells
- What is the main idea behind LSTMs and what is the main difference to simple RNN units?
  - simple RNN unit: hidden state is overridden each time: long-term memory bad
  - LSTMs have additional cell state which only gets multiplied/added to
  - forget and input gates control what goes in/out of cell memory
  - $C_t = f_t * C_{t-1} + i_t * ~C_t$
- What are the differences between LSTMs and GRUs?
  - GRUs have less parameters and wants to be easier to train
  - gates idea from LSTMs, but without cell state
  - reset gate: influence of previous hidden state
  - update gate: influence of update proposal on new hidden state
  - update proposal: calc update from reseted hidden state and input
- In which scenarios would LSTMs be beneficial compared to GRUs?
  - "Comparison between GRU and LSTM not conclusive, similar performance"
  - LSTMs have separated hidden and cell state, maybe good if importance of short- and longterm memory changes often in sequence? (*TODO: Is it though?*)
- Name three applications where many-to-one and one-to-many RNNs would be beneficial.
  1. many-to-one: recognise song/speaker
  2. many-to-one: tell if patient is healthy or not after having seen electrocardiogram
  3. one-to-many: given the first note of a song compose some music
  4. one-to-many: image captioning

## 8.: Visualization and Attention Mechanisms

- Why is visualization important? What can visualization help with? What can’t it help with?
  - Architecture visualization: Communicate Ideas
  - Training: Debugging, see dying ReLUs, vanishing/exploding gradients, ...
  - Parameters: Find reasons for unintuitive behavior, see what network focuses on, what features it thinks matter
- Why are confounds a problem?
  - Networks focuses on wrong effect, will fail to work on real data
  - Network will focus on the most discriminative features
  - If known they can be counteracted in the data or loss function
- Why could adversarial examples pose a security problem?
  - by adding noise not recognisable to human eye network can be tricked into seeing something else
- How does occlusion work?
  - Occlusion is about finding which part of an image causes a significant drop in prediction confidence
  - Move a masking patch around the image
  - used to find confounds and wrong focus
- What is the difference between deconvolution, backpropagation and guided backpropagation regarding feature visualization?
  - deconvolution: *TODO: Was genau macht das?* use reverse network
  - guided backpropagation: *TODO: Was genau macht das?* "Set all negative gradients in the backpropagation to zero"
- How is Google DeepDream related to visualization?
  - DeepDream/Inceptionsnism: change image so that it matches a class in backpropagation, not weights
  - will show new patterns in changed image (after a few iterations) showing what the network expects when it classifies something as a certain class
- What is a Saliency map?
  - shows impact of a pixel on the classification (might even show where something is located)
  - calculate gradient of classification loss w.r.t. image pixels
- What is the idea behind attention?
  - motivation: context, some information more important than other, remember related events
  - different parts of input relate to different parts of output
  - example: order of adjectives in languages like german and french reversed
  - Seq2Seq (e.g. translation):
    - split direct RNN into encoder and decoder RNNs, context vector passed from one network to the other
    - context vector weighted with alignment weights according to a score function
    - score function can trainable (e.g. FC layer)
- What is the difference between "normal" and self-attention
  - normal attention: computes attention of sequence to target/decoder RNN
  - self-attention: computes attention of sequence to itself
  - self-attention: enrich represenation of tokens with context info (e.g. what object is meant by "it")
- How does the transformer architecture avoid recurrence and convolutions?
  - "Attention is all you need"
  - allows for integration of knowledge independent of distance, faster training
  - AIAYN encoder:
    - translate input tokens into vector (of same length)
    - for each token compute: query `q`, key `k`, value `v` using trainable weights
    - alignment between query and key determines influence of `v`
    - multiple attention vectors per token, stacked attention blocks
  - AIAYN decoder: as encoder, but with additional input/output, self-attention only on previous outputs

## 9.: Deep Reinforcement Learning

- What is a policy?
  - prop. density function $pi(a)$ (or $pi(a|s)$ if state) describing which action to take (we want to optimize this)
  - in simple case: max. for a: $E[p(r|a)]$ (maximum expected reward over time)
  - in general: max. future return $max. g_t = \sum_{k=t+1}^T \gamma^{k-t-1} * r_k$ ($\gamma$ used to discount rewards in far future)
- What are value functions?
  - "action-value function" $Q_t(a)$ (changes with each new information, can also depend on state $s_t$)
  - incremental update possible: $Q_{t+1}(a) = Q_t(a) + (1/t)*(r_t - Q_t(a))$
- Explain the exploitation vs exploration dilemma.
  - comes up when training
  - greedy action selection: always choose same action with same input, use the one with most reward (never learns something new!)
  - need to sometimes do new things, explore new paths
  - in beginning of training explore a lot, later focus on exploitation
- Describe typical solutions to the dilemma.
  - uniform random choices: $pi(a) = 1 / |A|$
  - epsilon greedy: $pi(a) = a = max_a Q_t(a) ? 1 - eps : eps / (n - 1)$ (eps. influences how often to explore)
  - softmax: $pi(a) = e^{Q_t(a)/tau_t} / (\sum_{n=1}^{|A|} e^{Q_t(a_n)/tau_t})$ (decrease exploration over time using $tau$)
- What is the difference of a multi armed bandit problem to the full reinforcement learning problem?
  - "contextual bandit": we have an additional state which influences whats best
  - full reinforcement learning: additionally actions change state
- Describe a Markov decision process.
  - action $a_t \in A$
  - states $s_t \in S$
  - state transition pdf $p(s_{t+1} | s_t, a_t)$
  - reward $r_{t+1} \in R$ produced by transition
  - agents choose actions depending on policy
- Is an optimal policy necessarily unique?
  - there is only one optimal state-value function
  - there is only one optimal action-value function
  - any policy that results in optimal state-value function is fine
- What do the Bellman equations represent?
  - system of linear equations which can be used as iterative update rules
  - consistency conditions for state value function
  - __TODO__: What?
- Describe policy iteration.
  - introduce state-value function $V_pi(s)$ to guide search
  - update policy by iteratively updating state-value function using bellman equations
  - updated state-value-function means updated policy for greedy policy
  - iterate until policy stops changing
- Why does policy iteration work?
  - convergence is guaranteed for greedy selection because we only make choices so that $Q_pi(s, a) > V_pi(s)$
- How can you beat your friends in every Atari game?
  - yes: Deep Learning: Learn action-value function using deep network (input: vid. frames), output best next action of game
  - instead of label/cost-function update to max. reward
  - use eps. greedy policy with eps decreasing over training duration
- How can one master the game of Go?
  - AlphaGo has proven this!
  - Combine Supervised and Reinforcement Learning
  - Combine Monte-Carlo Tree Search (__TODO__: What?) with CNN
  - Multiple CNNs
    - Policy Network (propose move in leaf nodes for extension)
    - Value Network (predict chance of winning)
    - Rollout Policy Network (Guide rollout action selection)

## 10.: Unsupervised Deep Learning

- What is the defining characteristic of an autoencoder?
  - Autoencodes try to learn approx. of identity and do a dimensionality reduction
  - encoder: $y = f(x)$, decoder: $x' = g(y)$, train for $x = x'$
  - cut of encoder/decoder part in layer with fewer nodes than input had
  - variational autoencoders:
    - learn variational/probabilistic latent space variables
    - not simple category per variable but a probability distribution
    - similar latent space should produce similar output
    - encoder-part has "mean" and "variance" outputs per latent variable (randomly sample from this distribution)
    - problem: we cannot train a "randomness" node
    - solution: reparametrization: $z = mean + var * eps; eps  ~ Gauss(0, 1)$
    - can generate new data!
  - conditional GANs:
    - G-Net gets latent vector $z$ and conditioning vector $y$
    - D-Net gets $x$ and $y$
    - conditioning vector contains features like smiling, gender, age, ...
    - allows image to image translation (e.g. day to night)
- How do denoising autoencoders work?
  - DAEs implicitly estimate the underlying data-generating process
  - given noise model $C(x'|x)$ the DEA learns $p(x|x')$
- What does an optimal discriminator for GANs learn?
  - to separate fake from real images
  - $D*(x) = p_data(x) / (p_data(x) + p_model(x))$
- What are the advantages of GANs in comparison to other generative models?
  - generate samples in parallel (__TODO__: Wieso sollen andere das nicht können?)
  - few restrictions compared to Boltzman machines (*QnA-Lec.: No questions on Boltzman machines*)
- What is “mode collapse”?
  - Generator-Network in GAN rotates through modes of data, not converging
  - solutions
    - unrolled GANs (backpropagate through multiple steps of G-Net at once)
    - minibatch discrimination: minibatch for D considering the similarity of all samples to batch
- Explain feature matching/perceptual loss.
  - prevent “overtraining” of G on current D
  - G trained to match expected value of features f(x) of intermediate layer of D

## 11.: Segmentation and Object Detection

- What is the difference between semantic and instance segmentation and what is the connection to object detection?
  - object detection: instance recognition + localication
  - semantic instance segmentation: separate pixels of image of possibly same class
- How can we construct a network which accepts arbitrary input sizes?
  - crop/resize?
  - use fully convolutional network (filters do not care about actual size)
- What is ROI pooling?
  - ROI: "region of interest"
  - have a network find ROIs, call classification network only on those regions
  - improvements: calc some CNN features on hole image, share computation
  - ROI pooling: Pool acording to the proposed regions of interest
- How can we perform backpropagation through a ROI pooling layer?
  - __TODO__: ?
- What are typical measures for evaluation of segmentations?
  - Pixel Acc.: ratio between correctly classified and total number of pixels
  - Mean Pixel Acc.: Avg. Pixel Acc. per class
  - Mean Intersection over Union: ratio between intersection and union of pixel sets
  - Frequency Weighted Intersection over Union: MIOU weighted by freq. of each class
- What similarities do typical autoencoders share with typical segmentation networks?
  - "Hourglass" like look (go down and back up with layer sizes)
- YOLO: You only look once: use region proposal network for classification as well
  - divide image in SxS cells, per cell predict bounding boxes + confidence score, class confidence
  - build class probability map (per box)
  - combine bounding boxes+classes with class probability map
- Explain a method for instance segmentation.
  - Multi-Task-Loss: Segmentation-Loss + Box-Loss + Classification-Loss
  - 1.: region proposal
  - 2.: classification, bounding-box regression and segmentation in parallel
  - __TODO__: R-CNNs?

## 12.: Weakly and Self-Supervised Learning

*No comprehensive questions*

## 13.: Graph Convolutions

*No comprehensive questions*

## 14.: Known Operator Learning

*No comprehensive questions*
