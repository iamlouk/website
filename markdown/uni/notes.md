---
title: Notes
author: Lou Knauer
date: 12.10.2020
faicon: fa-notes
keywords:
  - lecture_notes
  - uni
---


## Some Lecture Notes

- [Deep Learning at FAU in the summer semester 2020](/uni/dl.html)
- *Rechnerarchitektur* (German)
  - [Old exam questions and answers](/uni/ra/fragen.html)
  - [Summary](/uni/ra/zusammenfassung.html)
- *Middleware - Cloud Computing* (German)
  - [Summary](/uni/mwcc/exam-prep.html)

