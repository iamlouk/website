---
title: Middleware - Cloud Computing (MW, FAU Erl.-Nbg, WS 2020/21) Zusammenfassung
author:
  - Lou Knauer
highlight-style: pygments
hidden: True
date: 06.03.2021
---

Links:

- [Vorlesungswebsite](https://www4.cs.fau.de/Lehre/WS20/V_MW/)
- [FSI Prüfungsprotokolle](https://fsi.cs.fau.de/dw/pruefungen/hauptstudium/ls4#middleware_couldcomputing_mwcc)

### Prüfungsfragen-Auswahl

Zusammenstellung der Prüfungsfragen bis zurück zu incl. 22.02.2018.

- Was ist Cloud Computing?
- Vor- und Nachteile von Clouds
- Virtualisierung
    - Wie funktioniert Virtualisierung?
    - Drei Eigenschaften von virtualisierten Systemen und ihre Bedeutung
    - Wie funktioniert Binary Translation?
    - Was macht man mit priviligierten und sensitiven Instruktionen bei Binary Translation?
- Daten-Clouds (Für große Daten)
    - Warum gehen bestätigte Daten in Microsoft Azure Storage ggf. trotzdem verloren?
    - Wie funktioniert schreiben im Stream-Layer von Azure Storage
    - Was passiert, wenn ein Extent-Node von Azure Storage ausfällt?
    - Was ist besonders, wenn der Primary Extent-Node von Azure Storage ausfällt, vergleiche es mit dem Ausfall eines Primary Chunkserver im Google File System?
    - Wie sieht die Topologie im GFS aus? Wie funktionieren Schreib-Aufrufe?
    - Warum brauchen wir einen Primary im GFS? Gehts auch ohne?
    - Wie wird der Primary im GFS bestimmt?
    - Was sind die Probleme mit einem zentrallen Master wie im GFS?
    - Wie kann es passieren, dass ein Teil einer Datei mehrfach in einem GFS-Chunk landet? Wie geht die Anwendung damit um? Warum findet sich in manchen Chunks Müll?
- Daten-Clouds (Für kleine Daten)
    - Warum und wie kann es passieren, dass Amazon Dynamo mehrere Datensätze zu einem Schlüssel zurückliefert?
    - Was passiert, wenn ein Knoten im Dynamo ausfällt?
    - Wie funktionieren Vector-Uhren und wie werden sie in Amazon Dynamo verwendet?
    - Wie kann es passieren, das man in Amazon Dynamo veraltete Daten liest?
    - Was ist und wo findet man "eventual consistency"?
- Koordinierung
    - Wie ist das mit der Konsistenz bei Zookeeper?
    - Warum hat Zookeeper zwei Zustände?
    - Wie stellt Chubby starke Konsistenz sicher?
    - Was passiert, wenn bei Chubby eine Netzwerkpartition entsteht?
- Datenverarbeitung
    - Wie ist der Datenfluss im MapReduce-Verfahren?
    - Wie teilen die Mapper den Reducern die Daten zu?
    - Wie kann man bei MapReduce Strom sparen?

## Web-Services

Ziel: Universeller Zugriff auf Cloud-Dienste (Sowohl von Anwendung zu anderen Anwendung als auch intern oder Administartionsschnittstellen)

Definition:

> ”A Web serviceis a software system designed to support interoperable machine-to-machine interaction over a network. It has an interface described in a machine-processable format (specifically WSDL). Other systems interact with the Web service in a manner prescribed by its description using SOAP messages, typically conveyed using HTTP with an XML serialization in conjunction with other Web-related standards.“

Quassi alle in der Vorlesung besprochenen Dienste haben solche Schnittstellen, vor allem eine REST-Variante.

#### HTTP

- Protokoll fürs www aufbauend auf TCP/IP
- Kennt verschiedene Metoden
    - `GET`: Lesender Zugriff
    - `HEAD`: Lesender Zugriff nur für HTTP-Header, leerer Body
    - `PUT`: Registriere Daten unter angegebener Addresse
    - `DELETE`: Lösche Daten
    - `POST`: Schicken von zu verarbeitenden Daten (z.B. Formulardaten)
- Antwort/Statuscodes
    - `100-199`: Kaum genutzt, Anfrage wurde empfangen aber wird noch bearbeitet
    - `200-299`: Anfrage erfolgreich abgearbeitet
    - `300-399`: Weiter/Umleitung auf anderen Dienst oder Endpunkt
    - `400-499`: Anfrage fehlerhaft
    - `500-599`: Server-Probleme, Anfrage ohne Schuld des Clients fehlgeschlagen

Anfrage-Beispiel:
```
POST /hello/world HTTP/1.1
X-Some-Header: Some-Key

Data...
```

Antwort-Beispiel:
```
HTTP/1.1 200 Ok
Server: Apache
Content-Type: text/html

Response-Data...
```

#### WSDL + SOAP

- Beschreibungssprache in XML für Funktionalität von Web-Services
- Zur automatisierten Erzeugung von Stubs für Zugriffe
- *Binding* zu Protokoll wie SOAP (HTTP __nur__ für Transport von SOAP-Nachrichten, alternativ z.B. SMTP)
- Nennt Endpunkte und Definitionen dieser für Anfragen

Beispiel:
```xml
<message name="getTermRequest">
  <part name="term" type="xs:string"/>
</message>

<message name="getTermResponse">
  <part name="value" type="xs:string"/>
</message>

<portType name="glossaryTerms">
  <operation name="getTerm">
    <input message="getTermRequest"/>
    <output message="getTermResponse"/>
  </operation>
</portType>

<binding type="glossaryTerms" name="b1">
   <soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http" />
   <operation>
     <soap:operation soapAction="http://example.com/getTerm"/>
     <input><soap:body use="literal"/></input>
     <output><soap:body use="literal"/></output>
  </operation>
</binding>
```

#### REST

- Zustandlose Kommunikation (Jede Nachricht ist für sich alleinstehend verarbeitbar)
- Anfragen und Antworten direkt über HTTP (Also Verwendung der HTTP-Statuscodes, Methoden, ...)
- Alles identifizierbar über *Univeral Resource Idenfitiers* (URIs)

## Virtualisierung

Normalerweise: Anwendung -> OS -> Hardware. Virtualisierung fügt einen *Virtual Machine Monitor* (VMM) bzw. Hypervisor
ein der zwischen diesen Schichten liegt (wie genau und wo hängt von der Art ab).

- __Systemvirtualiserung__: Ganze Hardware incl. ISA wird virtualisiert, z.B. via Paravirtualisierung (Xen)
- __Prozessvirtualiserung__: ABI wird nachgebaut, z.B. Betriebsyystemvirtualisierung

__Anforderungen an ein virtualisiertes System:__

1. Äquivalenz: Identisches Verhalten
2. Ressourcenkontrolle: VMM behält Kontrolle über Resourcen der VMs
3. Effizienz: So viel wie möglich die echte Hardware machen lassen

Letzteres Umsetzbar durch *Trap-and-Emulate*: Wenn VM etwas priviligiertes machen will,
dann kriegt das "echte" OS das mit, und der VMM emuliert diese Instruktion. VMM hält dafür
Schatten-Datenstrukturen etc.

Paravirtualisierung von Xen bricht etwas mit dem Äquivalenz-Prinzip, aus Perf. gründen.
VM muss für einige Dinge (Wie Page-Tables umsetzten) Hypercalls an VMM absetzten. Nachteil ist,
dass das Gast-OS angepasst werden muss.

Eine Alternative ist Betriebsystemvirt.:

- Beispiele: Docker, FreeBSD-Jails, Linux-VServer, ...
- Ein Kernel, viele Gast-VMs die nur im Userspace getrennt sind
- Häufig kombiniert mit Copy-on-Write-Dateisystemen, eigenen virtuellen Netzwerken, ...

## Cloud-Infrastrukturen

__*TODO!*__

## Verwaltung großer Datenmengen

__*TODO!*__

## Aufbau einer Datenspeicher-Cloud

__*TODO!*__

## Verwaltung kleiner Datensätze

__*TODO!*__

## Verarbeitung großer Datenmengen

__*TODO!*__

## Energieeffiziente Datenzentren

__*TODO!*__

## Koordinierungsdienste

__*TODO!*__

## Latenzminimierung in Datenzentren

__*TODO!*__

## Multi-Cloud Computing

__*TODO!*__

## Virtualisierungsbasierte Fehlertoleranz

__*TODO!*__
