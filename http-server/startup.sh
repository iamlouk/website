#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"
cd ..

if [ ! -f "./http-server/http-server.go" ]; then
	echo "run this script from the repo-root!"
	exit 1
fi

# if [ "$(id -u)" -eq 0 ]; then
#	echo "this script only exists so that you do NOT need to be root!"
#	exit 1
# fi

sudo iptables -t nat -I PREROUTING -p tcp --dport 80  -j REDIRECT --to-ports 8080 # HTTP
sudo iptables -t nat -I PREROUTING -p tcp --dport 443 -j REDIRECT --to-ports 8443 # HTTPS
sudo iptables -t nat -I PREROUTING -p tcp --dport 222 -j REDIRECT --to-ports 2022 # SSHAPPS Port #1
sudo iptables -t nat -I PREROUTING -p tcp --dport 21  -j REDIRECT --to-ports 2022 # FTP Port...

cd ./http-server
go build

cp /etc/letsencrypt/live/louknr.net/fullchain.pem .
cp /etc/letsencrypt/live/louknr.net/privkey.pem .

cd ..

exec sudo -u lou ./http-server/http-server --config ./config.yaml

