package main

import (
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"
	"encoding/json"
	"errors"
	"strings"
)

var client http.Client = http.Client{
	Timeout: 5 * time.Second,
}

type ipApiResponse struct {
	Status      string `json:"status"`
	Message     string `json:"message"`
	CountryCode string `json:"countryCode"`
	City        string `json:"city"`
	// ISP         string `json:"isp"`
	// AS          string `json:"as"`
}

func getLocation(r *http.Request) (loc string, err error) {
	ip := strings.Split(r.RemoteAddr, ":")[0]
	res, err := client.Get(fmt.Sprintf("http://ip-api.com/json/%s?fields=status,message,country,countryCode,regionName,city,timezone,isp,org,as,query", ip))
	if err != nil {
		return "", err
	}

	if res.StatusCode != 200 {
		return "", errors.New(res.Status)
	}

	var body ipApiResponse
	if err := json.NewDecoder(res.Body).Decode(&body); err != nil {
		return "", err
	}

	if body.Status != "success" {
		return body.Message, nil
	}

	return fmt.Sprintf("%s:%s", body.City, body.CountryCode), nil
	// return fmt.Sprintf("%s (%s), ISP: %s, AS: %s",
	//	body.City, body.CountryCode, body.ISP, body.AS), nil
}

var logfile io.Writer

type LogMsg struct {
	Request    *http.Request
	Time       time.Time
	StatusCode int
	Err        error
	Message    string
}

func logMsg(msg *LogMsg) {
	if msg.Time.IsZero() {
		msg.Time = time.Now()
	}

	buf := make([]byte, 0, 1024)
	buf = msg.Time.AppendFormat(buf, time.RFC3339)
	buf = append(buf, ' ')
	buf = append(buf, msg.Request.Method...)
	if msg.StatusCode != 0 {
		buf = append(buf, ' ')
		buf = strconv.AppendInt(buf, int64(msg.StatusCode), 10)
	}
	buf = append(buf, ' ')
	buf = append(buf, msg.Request.URL.Path...)
	if msg.Err != nil {
		buf = append(buf, ` err:"`...)
		buf = append(buf, msg.Err.Error()...)
		buf = append(buf, `"`...)
	}
	if msg.Message != "" {
		buf = append(buf, ` msg:"`...)
		buf = append(buf, msg.Message...)
		buf = append(buf, `"`...)
	}

	if msg.Request.URL.Path == "/" || msg.Request.URL.Path == "/index.html" {
		go func() {
			buf = append(buf, "\n--> "...)
			loc, err := getLocation(msg.Request)
			if err != nil {
				loc = err.Error()
			}
			buf = append(buf, `user-agent:"`...)
			buf = append(buf, msg.Request.UserAgent()...)
			buf = append(buf, "\" location:\""...)
			buf = append(buf, loc...)
			buf = append(buf, "\"\n"...)
			logfile.Write(buf)
		} ()
	} else {
		buf = append(buf, '\n')
		logfile.Write(buf)
	}
}
