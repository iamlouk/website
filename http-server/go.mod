module gitlab.com/iamlouk/website/http-server

go 1.19

require gopkg.in/yaml.v2 v2.4.0

require (
	github.com/creack/pty v1.1.21
	golang.org/x/crypto v0.16.0
	golang.org/x/exp v0.0.0-20231127185646-65229373498e
)

require golang.org/x/sys v0.15.0 // indirect
