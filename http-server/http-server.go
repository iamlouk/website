package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"strconv"
	"strings"

	"mime"
	"sync/atomic"
	"syscall"
	"time"

	"gitlab.com/iamlouk/website/http-server/ssh"
	"gopkg.in/yaml.v2"
)

type User struct {
	Name     string
	Password string
}

type Config struct {
	Hostname      string
	HttpEnabled   bool   `yaml:"http-enabled"`
	HttpPort      int    `yaml:"http-port"`
	HttpsEnabled  bool   `yaml:"https-enabled"`
	HttpsPort     int    `yaml:"https-port"`
	HttpsCertfile string `yaml:"https-cert"`
	HttpsKeyfile  string `yaml:"https-key"`
	WwwRoot       string `yaml:"www-root"`
	DataRoot      string `yaml:"data-root"`
	Logfile       string `yaml:"logfile"`
	Users         []User `yaml:"users"`
	SSHApps       *ssh.Config `yaml:"sshapps"`
}

var config Config

var totalRequests int32

func handleError(w http.ResponseWriter, r *http.Request, statusCode int, reason string) {
	logMsg(&LogMsg{Request: r, StatusCode: statusCode, Message: reason})
	w.WriteHeader(statusCode)
	if r.Method != http.MethodHead {
		fmt.Fprintf(w, "Error %d: %s\n", statusCode, reason)
	}
}

func serveFile(w http.ResponseWriter, r *http.Request, fpath string, finfo os.FileInfo) {
	f, err := os.Open(fpath)
	if err != nil {
		if os.IsNotExist(err) {
			handleError(w, r, http.StatusNotFound, err.Error())
			return
		}

		if os.IsPermission(err) {
			handleError(w, r, http.StatusForbidden, err.Error())
			return
		}

		handleError(w, r, http.StatusInternalServerError, err.Error())
		return
	}

	now := time.Now()
	logMsg(&LogMsg{Request: r, Time: now, StatusCode: 200})

	ext := filepath.Ext(fpath)
	mimetype := mime.TypeByExtension(ext)

	if mimetype != "" {
		w.Header().Set("Content-Type", mimetype)
	}
	w.Header().Set("Content-Length", strconv.FormatInt(finfo.Size(), 10))
	w.Header().Set("Last-Modified", finfo.ModTime().Format(http.TimeFormat))
	// w.Header().Set("Cross-Origin-Opener-Policy", "same-origin")
	// w.Header().Set("Cross-Origin-Embedder-Policy", "require-corp")
	w.Header().Set("Server", "go net/http")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(http.StatusOK)

	defer f.Close()

	if r.Method != http.MethodHead {
		_, err = io.Copy(w, f)
		if err != nil {
			log.Print(err)
			return
		}
	}
}

/*
 * Default handle for everything except the data directory.
 */
func handler(w http.ResponseWriter, r *http.Request) {
	atomic.AddInt32(&totalRequests, 1)

	if r.Method != http.MethodGet && r.Method != http.MethodHead {
		w.Header().Set("Allow", "GET, HEAD")
		handleError(w, r, http.StatusMethodNotAllowed, "Bad method")
		return
	}

	fpath := path.Join(config.WwwRoot, r.URL.Path)
	if !strings.HasPrefix(fpath, config.WwwRoot) {
		handleError(w, r, http.StatusForbidden, "path would leave www-root")
		return
	}

	finfo, err := os.Lstat(fpath)
	if err != nil {
		if os.IsNotExist(err) {
			if finfo, err2 := os.Stat(path.Join(config.WwwRoot, "404.html")); err2 == nil && finfo.Mode().IsRegular() {
				w.WriteHeader(http.StatusNotFound)
				f, err := os.Open(path.Join(config.WwwRoot, "404.html"))
				if err != nil {
					log.Print(err)
					return
				}
				defer f.Close()
				_, err = io.Copy(w, f)
				if err != nil {
					log.Print(err)
				}
				return
			}

			handleError(w, r, http.StatusNotFound, err.Error())
		} else if os.IsPermission(err) {
			handleError(w, r, http.StatusForbidden, err.Error())
		} else {
			handleError(w, r, http.StatusInternalServerError, err.Error())
		}
		return
	}

	if finfo.Mode()&os.ModeSymlink != 0 {
		fpath, err = filepath.EvalSymlinks(fpath)
		if err != nil {
			handleError(w, r, http.StatusInternalServerError, err.Error())
			return
		}

		fpath, err = filepath.Abs(fpath)
		if err != nil {
			handleError(w, r, http.StatusInternalServerError, err.Error())
			return
		}

		if strings.HasPrefix(fpath, config.WwwRoot) {
			http.Redirect(w, r, fpath[len(config.WwwRoot):], http.StatusMovedPermanently)
			return
		}

		handleError(w, r, http.StatusForbidden, "symlink would leave www-root")
		return
	}

	if finfo.IsDir() {
		if !strings.HasSuffix(r.URL.Path, "/") {
			http.Redirect(w, r, fmt.Sprintf("%s/", r.URL.Path), http.StatusMovedPermanently)
			return
		}

		indexHtmlPath := path.Join(fpath, "/index.html")
		if finfo, err := os.Stat(indexHtmlPath); err == nil && finfo.Mode().IsRegular() {
			serveFile(w, r, indexHtmlPath, finfo)
			return
		}

		fmt.Fprintf(w, `<!DOCTYPE html>
			<html lang=\"en\">
			<head><meta charset=\"utf-8\"/><title>%s</title></head>
			<body>
				<h1>%s</h1><ul>`, r.URL.Path, r.URL.Path)
		err := filepath.Walk(fpath, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			path = path[len(config.WwwRoot):]
			_, err = fmt.Fprintf(w, "<li><a href=\"%s\">%s</a></li>\n", path, path)
			return err
		})
		if err != nil {
			log.Print(err)
			return
		}
		fmt.Fprint(w, "</ul></body></html>\n")
		return
	}

	if !finfo.Mode().IsRegular() {
		handleError(w, r, http.StatusInternalServerError, "unhandled file mode")
		return
	}

	serveFile(w, r, fpath, finfo)
}

func checkAuthentication(w http.ResponseWriter, r *http.Request) bool {
	username, password, ok := r.BasicAuth()
	if !ok {
		return false
	}

	for _, user := range config.Users {
		if user.Name == username && user.Password == password {
			return true
		}
	}

	return false
}

/*
 * When using the correct username/password, clients can
 * get, put or delete files in `/data`.
 */
func dataHandler(w http.ResponseWriter, r *http.Request) {
	atomic.AddInt32(&totalRequests, 1)

	if r.URL.Scheme != "https" {
		http.Redirect(w, r, r.URL.Path, http.StatusMovedPermanently)
		return
	}

	if !checkAuthentication(w, r) {
		w.Header().Set("WWW-Authenticate", "Basic realm=\"Read/Write Access to /data\"")
		handleError(w, r, http.StatusUnauthorized, "please authenticate")
		return
	}

	fpath := path.Join(config.DataRoot, r.URL.Path[len("/data/"):])
	logMsg(&LogMsg{Request: r})

	switch r.Method {
	case http.MethodPut:
		directory := filepath.Dir(fpath)
		err := os.MkdirAll(directory, 0755)
		if err != nil {
			handleError(w, r, http.StatusInternalServerError, err.Error())
			return
		}

		f, err := os.Create(fpath)
		if err != nil {
			handleError(w, r, http.StatusInternalServerError, err.Error())
			return
		}

		w.WriteHeader(http.StatusOK)

		defer f.Close()
		_, err = io.Copy(f, r.Body)
		if err != nil {
			log.Print(err)
		}
		return

	case http.MethodGet, http.MethodHead:
		finfo, err := os.Lstat(fpath)
		if err == nil && !finfo.Mode().IsRegular() {
			handleError(w, r, http.StatusForbidden, "this is a directory")
			return
		}
		serveFile(w, r, fpath, finfo)
		return

	case http.MethodDelete:
		err := os.Remove(fpath)
		if err != nil {
			handleError(w, r, http.StatusInternalServerError, err.Error())
			return
		}
		w.WriteHeader(http.StatusOK)
		return

	default:
		w.Header().Set("Allow", "GET, HEAD, PUT, DELETE")
		handleError(w, r, http.StatusMethodNotAllowed, "Bad Method")
		return
	}
}

func normalizePath(path string) string {
	npath, err := filepath.EvalSymlinks(path)
	if err != nil {
		log.Fatal(err)
	}

	npath, err = filepath.Abs(npath)
	if err != nil {
		log.Fatal(err)
	}

	return npath
}

func main() {
	config = Config{HttpEnabled: true}

	flag.IntVar(&config.HttpPort, "port", 8080, "port to listen on (http)")
	flag.StringVar(&config.WwwRoot, "www", ".", "directory to serve")
	flag.StringVar(&config.DataRoot, "data", "", "directory you can http GET/PUT/DELETE to when path start with /data")

	var configFile string
	flag.StringVar(&configFile, "config", "", "instead of all the command line parameters, provide a YAML config file")

	flag.Parse()

	if configFile != "" {
		b, err := os.ReadFile(configFile)
		if err != nil {
			log.Fatal(err)
		}

		err = yaml.Unmarshal(b, &config)
		if err != nil {
			log.Fatal(err)
		}
	}

	if config.Logfile == "-" {
		logfile = os.Stderr
	} else if config.Logfile != "" {

		f, err := os.OpenFile(config.Logfile, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		logfile = f
	} else {
		logfile = io.Discard
	}

	config.WwwRoot = normalizePath(config.WwwRoot)
	config.DataRoot = normalizePath(config.DataRoot)

	fmt.Printf("Serving '%s' on /...\n", config.WwwRoot)

	if config.DataRoot != "" {
		username := os.Getenv("HTTP_USERNAME")
		password := os.Getenv("HTTP_PASSWORD")

		if username != "" {
			config.Users = append(config.Users, User{Name: username, Password: password})
		}

		http.HandleFunc("/data/", dataHandler)
		fmt.Printf("Serving '%s' on /data/...\n", config.DataRoot)
	}
	http.HandleFunc("/", handler)

	if config.HttpEnabled {
		go func() {
			log.Fatal(http.ListenAndServe(fmt.Sprintf("0.0.0.0:%d", config.HttpPort), nil))
		}()
		fmt.Printf("Starting HTTP Server on Port %d\n", config.HttpPort)
	}

	if config.HttpsEnabled {
		go func() {
			log.Fatal(http.ListenAndServeTLS(fmt.Sprintf("0.0.0.0:%d", config.HttpsPort),
				config.HttpsCertfile, config.HttpsKeyfile, nil))
		}()
		fmt.Printf("Starting HTTPS Server on Port %d\n", config.HttpsPort)
	}

	if config.SSHApps != nil {
		go func() {
			if err := ssh.RunServer(context.Background(), config.SSHApps); err != nil {
				log.Fatal(err)
			}
		}()
	}

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)
	<-signals
	fmt.Printf("\nShuting down: total requests: %d\n", atomic.LoadInt32(&totalRequests))
}
