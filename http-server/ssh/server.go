package ssh

import (
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/exec"
	"sync"
	"syscall"
	"unsafe"

	"github.com/creack/pty"
	"golang.org/x/crypto/ssh"
	"golang.org/x/exp/slices"
)

// Partly based on: https://gist.github.com/jpillora/b480fde82bff51a06238

type App struct {
	Name    string   `yaml:"name"`
	Command string   `yaml:"command"`
	Args    []string `yaml:"args"`
}

type Config struct {
	Port                int16  `yaml:"port"`
	HostKeyFile         string `yaml:"host-key-file"`
	Password            string `yaml:"password"`
	AuthorizedHostsFile string `yaml:"authorized-hosts-file"`
	Apps                []App  `yaml:"apps"`
}

func RunServer(ctx context.Context, config *Config) error {
	if config.HostKeyFile == "" {
		config.HostKeyFile = fmt.Sprintf("%s/.ssh/id_rsa", os.Getenv("HOME"))
	}
	if config.AuthorizedHostsFile == "" {
		config.AuthorizedHostsFile = fmt.Sprintf("%s/.ssh/known_hosts", os.Getenv("HOME"))
	}

	bytes, err := os.ReadFile(config.HostKeyFile)
	if err != nil {
		return fmt.Errorf("failed to read %s: %w", config.HostKeyFile, err)
	}

	privateKey, err := ssh.ParsePrivateKey(bytes)
	if err != nil {
		return fmt.Errorf("failed to parse private key %s: %w", config.HostKeyFile, err)
	}

	authorizedKeys := map[string]bool{}
	if len(config.AuthorizedHostsFile) > 0 {
		bytes, err := os.ReadFile(config.AuthorizedHostsFile)
		if err != nil {
			return fmt.Errorf("failed to parse authorized keys from %s: %w",
				config.AuthorizedHostsFile, err)
		}

		for len(bytes) > 0 {
			publicKey, _, _, nextline, err := ssh.ParseAuthorizedKey(bytes)
			if err != nil {
				return fmt.Errorf("failed to parse authorized keys from %s: %w",
					config.AuthorizedHostsFile, err)
			}

			authorizedKeys[string(publicKey.Marshal())] = true
			bytes = nextline
		}
	}

	sshconfig := &ssh.ServerConfig{}
	if len(authorizedKeys) > 0 {
		callback := func(conn ssh.ConnMetadata, key ssh.PublicKey) (*ssh.Permissions, error) {
			if !authorizedKeys[string(key.Marshal())] {
				return nil, errors.New("unknown key")
			}
			return nil, nil
		}
		sshconfig.PublicKeyCallback = callback
	}
	if len(config.Password) > 0 {
		callback := func(conn ssh.ConnMetadata, password []byte) (*ssh.Permissions, error) {
			if config.Password != string(password) {
				return nil, errors.New("wrong password")
			}
			return nil, nil
		}
		sshconfig.PasswordCallback = callback
	}
	if len(authorizedKeys) == 0 && len(config.Password) == 0 {
		log.Printf("SSH: Warning: Allowing auth. with no keys nor password!")
		sshconfig.NoClientAuth = true
	}
	sshconfig.AddHostKey(privateKey)

	log.Printf("[SSH]: Listening on port %d", config.Port)
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", config.Port))
	if err != nil {
		return err
	}

	go func() {
		_, _ = <-ctx.Done()
		if err := listener.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	for {
		tcpconn, err := listener.Accept()
		if err != nil {
			if errors.Is(err, net.ErrClosed) {
				break
			}
			log.Printf("[SSH]: accept failed: %s", err.Error())
			continue
		}

		sshconn, chans, reqs, err := ssh.NewServerConn(tcpconn, sshconfig)
		if err != nil {
			log.Printf("[SSH]: handshake failed: %s", err.Error())
			tcpconn.Close()
			continue
		}

		log.Printf("[SSH]: New connection from %s (%s), user: '%s'",
			sshconn.RemoteAddr().String(),
			string(sshconn.ClientVersion()),
			sshconn.User())
		go ssh.DiscardRequests(reqs)

		appidx := slices.IndexFunc(config.Apps, func(app App) bool {
			return app.Name == sshconn.User()
		})
		if appidx == -1 {
			log.Print("[SSH]: No app for username found.")
			sshconn.Close()
			continue
		}

		go func() {
			for c := range chans {
				go handleSSHChannel(sshconn, c, &config.Apps[appidx])
			}
		}()
	}

	return nil
}

func handleSSHChannel(sshconn *ssh.ServerConn, channel ssh.NewChannel, app *App) {
	if t := channel.ChannelType(); t != "session" {
		log.Printf("[SSH] unsupported channel type: %s", t)
		channel.Reject(ssh.UnknownChannelType, fmt.Sprintf("unknown channel type: %s", t))
		return
	}

	conn, reqs, err := channel.Accept()
	if err != nil {
		log.Printf("[SSH] channel accept failed: %s", err.Error())
		sshconn.Close()
		return
	}

	ws := &WindowSize{}
	cmd := exec.Command(app.Command, app.Args...)
	var tty *os.File

	var once sync.Once
	close := func() {
		sshconn.Close()
		if tty != nil && cmd.Process != nil {
			cmd.Process.Kill()
		}
		log.Printf("[SSH] connection closed (%s)", sshconn.RemoteAddr().String())
	}
	defer once.Do(close)

	for req := range reqs {
		switch req.Type {
		case "shell":
			ok := len(req.Payload) == 0 && tty == nil
			if !ok {
				log.Printf("[SSH] Non-default shell requested: %#v", string(req.Payload))
				// req.Reply(false, nil)
				// return
				continue
			}

			tty, err = pty.StartWithSize(cmd, &pty.Winsize{ Rows: uint16(ws.Height), Cols: uint16(ws.Width) })
			if err != nil {
				log.Printf("[SSH] pty.Start failed: %s", err.Error())
				req.Reply(false, []byte(err.Error()))
				return
			}

			go func(){
				io.Copy(conn, tty)
				once.Do(close)
			}()
			go func(){
				io.Copy(tty, conn)
				once.Do(close)
			}()

			log.Printf("[SSH] shell request and command started!")
			req.Reply(true, nil)
		case "pty-req":
			term, buf, _ := parseString(req.Payload)
			buf, ok := ws.Parse(buf)
			if !ok {
				req.Reply(false, nil)
				continue
			}
			cmd.Env = append(cmd.Environ(), fmt.Sprintf("TERM=%s", term))
			log.Printf("[SSH] pty-req request (%dx%d): %#v", ws.Width, ws.Height, term)
		case "window-change":
			_, ok := ws.Parse(req.Payload)
			if !ok || tty != nil {
				req.Reply(false, nil)
				continue
			}
			log.Printf("[SSH] window-change request: %dx%d", ws.Width, ws.Height)
			ws.Set(tty.Fd())
			req.Reply(true, nil)
		default:
			log.Printf("[SSH] %#v request: unknown (len(payload)=%d)!",
				req.Type, len(req.Payload))
		}
	}
}

func parseString(in []byte) (string, []byte, bool) {
	length, tail, ok := parseUint32(in)
	if !ok || uint32(len(tail)) < length {
		return "", nil, false
	}

	return string(tail[:length]), tail[length:], true
}

func parseUint32(in []byte) (uint32, []byte, bool) {
	if len(in) < 4 {
		return 0, nil, false
	}
	return binary.BigEndian.Uint32(in), in[4:], true
}

type WindowSize struct {
	Height, Width uint32
}

func (ws *WindowSize) Parse(buf []byte) ([]byte, bool) {
	var ok1, ok2 bool
	ws.Width, buf, ok1 = parseUint32(buf)
	ws.Height, buf, ok2 = parseUint32(buf)
	return buf, ok1 && ok2
}

func (ws *WindowSize) Set(fd uintptr) {
	data := make([]byte, 0, 8)
	data = binary.NativeEndian.AppendUint16(data, uint16(ws.Width))
	data = binary.NativeEndian.AppendUint16(data, uint16(ws.Height))
	data = append(data, 0, 0, 0, 0)
	syscall.Syscall(syscall.SYS_IOCTL, fd,
		uintptr(syscall.TIOCSWINSZ),
		uintptr(unsafe.Pointer(&data[0])))
}
