#!/usr/bin/env python

import os
import sys
import yaml
import requests
from requests.auth import HTTPBasicAuth

if len(sys.argv) != 2:
    print(f"usage: {sys.argv[0]} path", file=sys.stderr)
    sys.exit(1)

projdir = os.path.dirname(os.path.realpath(__file__))
with open(f"{projdir}/../config.yaml") as f:
    config = yaml.safe_load(f)

auth = HTTPBasicAuth(config['users'][0]['name'], config['users'][0]['password'])
path = sys.argv[1]
url = f"{config['protocol']}://{config['hostname']}:{config['port']}/data/{path}"
headers = { 'Connection': 'close' }

s = requests.session()
s.keep_alive = False

if "get" in sys.argv[0]:
    make_req = lambda: requests.get(url, auth=auth, headers=headers)
elif "del" in sys.argv[0]:
    make_req = lambda: requests.delete(url, auth=auth, headers=headers)
elif "put" in sys.argv[0]:
    data = b"hello world"
    make_req = lambda: requests.put(url, data=data, auth=auth, headers=headers)
else:
    print(f"the program name ('{sys.argv[0]}') needs to contain 'put', 'get' or 'del'", file=sys.stderr)
    sys.exit(1)

try:
    res = make_req()
    res.connection.close()
except Exception as e:
    print(f"error: {e}", file=sys.stderr)
    sys.exit(1)

if res.status_code != 200:
    print(f"error {res.status_code}: {res.reason}", file=sys.stderr)
    sys.exit(1)

print(res.text)

