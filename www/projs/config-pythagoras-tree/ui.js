var panel = ({

  $el: $('#panel'),
  $props: $('#panel #props'),
  changeTimeoutId: null,
  timeout: 500,
  $rendering: $('#rendering'),
  $anims: $('#anims'),
  $sizes: $('#size'),
  isHidden: true,

  toggle: function(){
    if (this.isHidden) {
      this.$el.fadeIn(250);
      this.isHidden = false;
    } else {
      this.$el.fadeOut(250);
      this.isHidden = true;
    }
  },

  init: function(){
    document.addEventListener('keydown', function(event){
      if (event.keyCode === 32) {
        panel.toggle();
        event.preventDefault();
      }
    }, false);

    this.$el.find('#rerender').on('click', function(){ panel.rerender(); });

    this.$autoRender = this.$el.find('#do-auto-render');

    this.$props.on('change', function(){
      if (panel.$autoRender.is(':checked')) {
        if (panel.changeTimeoutId != null) clearTimeout(panel.changeTimeoutId);

        panel.changeTimeoutId = setTimeout(panel.rerender.bind(panel), panel.timeout);
      }
    });

    this.$el.find('#do-animation').on('change', function(){
      var $this = $(this);
      if ($this.is(':checked')) {
        panel.validateAnimationProps();
        panel.validateProps();
        animation.start();
      } else {
        animation.stop();
        panel.rerender();
      }
    });

    this.$anims.on('change', function(){
      panel.validateAnimationProps();
      animation.updateProps();
    });

    this.$sizes.on('change', function(){
      panel.resize();
      panel.rerender();
    });

    return this;
  },

  rerender: function(){
    this.validateProps();
    if (animation.paused === true) {
      this.$rendering.show();
      setTimeout(() => {
        render();
        this.$rendering.hide();
      }, 10);
    }
  },

  validateProps: function(){
    props['zeichnen']['linie-l'] = this.$props.find('#draw-line-l').is(":checked");
    props['zeichnen']['linie-r'] = this.$props.find('#draw-line-r').is(":checked");
    props['zeichnen']['rechteck'] = this.$props.find('#draw-square').is(":checked");
    props['zeichnen']['linie-PzuP'] = this.$props.find('#draw-line').is(":checked");

    props['faktor'] = Number.parseInt(this.$props.find('#slider-faktor').val()) * 0.001;
    props['anzahl-rek-aufrufe'] = Number.parseInt(this.$props.find('#slider-i').val());
    props['start-breite'] = canvas.height * 0.04 * Number.parseInt(this.$props.find('#slider-breite').val()) * 0.1;
    props['höhen-breiten-verhältnis'] = Number.parseInt(this.$props.find('#slider-v-hoehe-breite').val()) * 0.1;
    props['linien-dicke'] = Number.parseInt(this.$props.find('#slider-linien-dick').val()) * 0.1;
    props['farbe2ab'] = Number.parseInt(this.$props.find('#slider-farbe2ab').val());

    props['farbe1'] = this.$props.find('#color1').val();
    props['farbe2'] = this.$props.find('#color2').val();
    props['hintergrund'] = this.$props.find('#bg-color').val();
  },

  validateAnimationProps: function(){
    props['animation']['step'] = Number.parseInt(this.$anims.find('#anim-step').val()) * 0.00002 * 0.0075;
    props['animation']['d'] = Number.parseInt(this.$anims.find('#anim-d').val()) * 0.00002 * 0.080;
  },

  org_canvas_width: canvas.width,
  org_canvas_height: canvas.height,
  resize: function(w, h){
    canvas.width =  w || (this.org_canvas_width  * Number.parseInt(this.$sizes.find('#size-x').val()) * 0.1);
    canvas.height = h || (this.org_canvas_height * Number.parseInt(this.$sizes.find('#size-y').val()) * 0.1);

    canvas.style.width = canvas.width+'px';
    canvas.style.height = canvas.height+'px';

    console.log(canvas.width, canvas.height);
  }

  // export: function(){
  //   prompt("Export:", JSON.stringify(props));
  // },
  //
  // import: function(){
  //   var json = JSON.parse( prompt("Import...?") );
  //
  //   this.$props.find('#draw-line-l').prop('checked', json['zeichnen']['linie-l']);
  //   this.$props.find('#draw-line-r').prop('checked', json['zeichnen']['linie-r']);
  //   this.$props.find('#draw-square').prop('checked', json['zeichnen']['rechteck']);
  //   this.$props.find('#draw-line').prop('checked', json['zeichnen']['linie-PzuP']);
  //
  //   this.$props.find('#slider-faktor').val((1 / 0.001) * json['faktor']);
  // }

}).init();
