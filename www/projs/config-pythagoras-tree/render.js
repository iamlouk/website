var canvas = document.querySelector('#canvas');
canvas.width = canvas.offsetWidth; //document.body.offsetWidth;
canvas.height = canvas.offsetHeight; //document.body.offsetHeight;

panel.org_canvas_width = canvas.width;
panel.org_canvas_height = canvas.height;

var ctx = canvas.getContext('2d');
ctx.lineWidth = 1;
ctx.strokeStyle = '#535353';

var props = ({
  'höhen-breiten-verhältnis': 1,
  'anzahl-rek-aufrufe': 10,
  'start-breite':(canvas.height * 0.175),
  'faktor': 0.5,
  'zeichnen':{
    'rechteck': false,
    'linie-l': false,
    'linie-r': false,
    'linie-PzuP': false
  },
  'farbe1': '#000',
  'farbe2': '#000',
  'farbe2ab': 7,
  'linien-dicke': 2,
  'hintergrund': '#ffffff',
  'animation':{
    'step': 0.0075,
    'd': 0.080
  }
});

var animation = ({
  x: 0,
  y: null,
  step: 0.0075,
  d: 0.080,
  paused: true,
  animationFrameId: null,
  $fps: document.querySelector('#fps'),
  fps: 0,

  updateProps: function(){
    this.step = props['animation']['step'];
    this.d = props['animation']['d'];
  },

  calcNextY: function(){
    this.x += this.step;
    this.y = 0.5 + (Math.sin(this.x) * this.d);
    return this.y;
  },

  onFrame: function(){
    this.animationFrameId = window.requestAnimationFrame( this.onFrame.bind(this) );
    if (this.paused === false) {
      this.calcNextY();
      render(this.y);
      this.fps += 1;
    }
  },

  init: function(){
    setInterval(() => {
      if (this.paused === false) {
        this.$fps.innerHTML = ("FPS: "+this.fps);
        this.fps = 0;
      }
    }, 1000);
    return this;
  },

  start: function(){
    this.paused = false;
    this.animationFrameId = window.requestAnimationFrame( this.onFrame.bind(this) );
    $(this.$fps).show();
  },
  stop:  function(){
    this.paused = true;
    window.cancelAnimationFrame( this.animationFrameId );
    $(this.$fps).hide();
  }

}).init();

function render(faktor){
  // if (props['anzahl-rek-aufrufe'] > 14 &&
  //     props['zeichnen']['rechteck']) {
  //   alert("Anzahl an Zeichnungen zu hoch!");
  //   animation.paused = true;
  //   return;
  // }

  var timestamp = Date.now();

  //ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = props['hintergrund'];
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  //$(document.body).css({ 'background-color':props['hintergrund'] });

  ctx.lineWidth = props['linien-dicke'];

  var P = new Point(canvas.width / 2, canvas.height+10);
  var square = new Square(
    new Point((canvas.width / 2) - (props['start-breite'] / 2), canvas.height),
    new Vector(0, -props['start-breite'] * props['höhen-breiten-verhältnis']),
    new Vector(props['start-breite'], 0)
  );

  // ctx.beginPath();
  zeichnen(props['anzahl-rek-aufrufe'], square, P, faktor || props['faktor'], 'l');
  // ctx.stroke();

  //console.info("Gerendert in", (Date.now() - timestamp), "ms");
  ctx.restore();
};

function zeichnen(i, square, lastP, faktor){
  if (i === 0) return;

  var P = Geo.getPointOnCircle(square.B.clone(), square.B.addVec(square.BD), faktor);


  if (i < (props['anzahl-rek-aufrufe'] - props['farbe2ab'])) {
    ctx.strokeStyle = props['farbe1'];
  } else {
    ctx.strokeStyle = props['farbe2'];
  }


  if (props['zeichnen']['rechteck']) {
    square.draw(ctx);
  }
  if (props['zeichnen']['linie-l']) {
    ctx.beginPath();
    ctx.moveTo(lastP.x, lastP.y);
    ctx.quadraticCurveTo(square.D.x, square.D.y, P.x, P.y);
    ctx.stroke();
  }
  if (props['zeichnen']['linie-r']) {
    ctx.beginPath();
    ctx.moveTo(lastP.x, lastP.y);
    ctx.quadraticCurveTo(square.B.x, square.B.y, P.x, P.y);
    ctx.stroke();
  }
  if (props['zeichnen']['linie-PzuP']) {
    ctx.beginPath();
    ctx.moveTo(lastP.x, lastP.y);
    ctx.lineTo(P.x, P.y);
    ctx.stroke();
  }

  var leftSquare = Square.fromPointsLeft(square.B, P, props['höhen-breiten-verhältnis']);
  var rightSquare = Square.fromPointsRight(square.D, P, props['höhen-breiten-verhältnis']);

  zeichnen(i-1, leftSquare, P, faktor, 'r');
  zeichnen(i-1, rightSquare, P, faktor, 'l');
};

(function(){
  var zeichnen = function(i, previousP, square, side){
    if (i === 0) return;

    var P = Geo.getPointOnCircle(square.B.clone(), square.B.addVec(square.BD), Geo.random(0.45, 0.55));

    if (i < 7) {
      ctx.strokeStyle = '#92e023';
      ctx.lineWidth = 1;
    } else {
      ctx.strokeStyle = '#992600';
      ctx.lineWidth = i-8;
    }

    if (side === 'r') {
      ctx.beginPath();
      ctx.moveTo(previousP.x, previousP.y);
      ctx.quadraticCurveTo(square.D.x, square.D.y, P.x, P.y);
      ctx.stroke();
    } else {
      ctx.beginPath();
      ctx.moveTo(previousP.x, previousP.y);
      ctx.quadraticCurveTo(square.B.x, square.B.y, P.x, P.y);
      ctx.stroke();
    }

    var leftSquare = Square.fromPointsLeft(square.B, P, Geo.random(0.9, 1.1));
    var rightSquare = Square.fromPointsRight(square.D, P, Geo.random(0.9, 1.1));

    zeichnen(i-1, P, leftSquare, 'l');
    zeichnen(i-1, P, rightSquare, 'r');
  };

  if (canvas.width > 300) {
    ctx.fillStyle = '#000000';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    panel.$rendering.show();

    var width = canvas.width * 0.15;
    var height = width;

    var P = new Point(canvas.width / 2, canvas.height+10);

    var wh = Math.min(canvas.height, canvas.width) / 6;

    var square = new Square(
      new Point((canvas.width / 2) - (width / 2), canvas.height),
      new Vector(0, -wh),
      new Vector(wh, 0)
    );

    document.body.style.backgroundColor = '#171717';
    zeichnen(17, P, square);
    panel.$rendering.hide();
  } else {
    panel.rerender();
  }

})();
