var Point = function(x, y){
  this.x = x || 0;
  this.y = y || 0;
};

Point.prototype.add = function(x, y){
  return new Point(
    this.x + x,
    this.y + y
  );
};

Point.prototype.addVec = function(vec){
  return new Point(
    this.x + vec.x,
    this.y + vec.y
  );
};

Point.prototype.draw = function(ctx, color){
  if (color) {
    var temp = ctx.strokeStyle;
    ctx.strokeStyle = color;
  }

  ctx.beginPath();
  ctx.arc(this.x, this.y, 5, 0, 2 * Math.PI, false);
  ctx.stroke();

  if (color)
    ctx.strokeStyle = temp;
};

Point.prototype.moveBy = function(vec){
  this.x += vec.x;
  this.y += vec.y;
  return this;
};

Point.prototype.clone = function(){
  return new Point(this.x, this.y);
};

/*>------<*/

var Vector = function(x, y){
  this.x = x || 0;
  this.y = y || 0;
};

Vector.fromPoints = function(A, B){
  return new Vector(B.x - A.x, B.y - A.y);
};

Vector.prototype.length = function(){
  return Math.sqrt( this.x * this.x + this.y * this.y );
};

Vector.prototype.vertical = function(){
  return new Vector(
    this.y,
    -this.x
  );
};

Vector.prototype.multiply = function(by){
  this.x = this.x * by;
  this.y = this.y * by;
  return this;
};

Vector.prototype.draw = function(P, ctx){
  ctx.beginPath();
  ctx.moveTo(P.x, P.y);
  ctx.lineTo(P.x + this.x, P.y + this.y);
  ctx.stroke();
};

Vector.prototype.getPointOnLine = function(P, f){
  P.moveBy( this.multiply(f) );
  return P;
};

Vector.prototype.clone = function(){
  return new Vector(this.x, this.y);
};

Vector.prototype.unitVec = function(){
  var l = this.length();
  return new Vector(
    this.x / l,
    this.y / l
  );
};

/*>------<*/

var Square = function(A, AB, AC){
  this.A = A || new Point(0, 0);

  this.AB = AB || new Vector(0, 0);
  this.AC = AC || new Vector(0, 0);

  this.B = this.A.add(this.AB.x, this.AB.y);
  this.C = this.A.add(this.AC.x, this.AC.y);
  this.D = this.B.add(this.AC.x, this.AC.y);

  this.BD = Vector.fromPoints(this.B, this.D);
  this.CD = Vector.fromPoints(this.C, this.D);
};

Square.fromPointsLeft = function(A, B, widthHeight){

  var AC = Vector.fromPoints(A, B);
  var AB = AC.vertical().multiply(widthHeight);

  return new Square(A, AB, AC);

};

Square.fromPointsRight = function(A, B, widthHeight){

  var AC = Vector.fromPoints(A, B).multiply(-1);
  var AB = AC.vertical().multiply(widthHeight);

  return new Square(B, AB, AC);

};

Square.prototype.draw = function(ctx){

  this.AB.draw(this.A, ctx);
  this.AC.draw(this.A, ctx);
  this.BD.draw(this.B, ctx);
  this.CD.draw(this.C, ctx);

  return this;
};

/*>------<*/

var Geo = ({

  random: function(min, max){
    return min + (Math.random() * (max-min));
  },

  getPointOnCircle: function(A, B, faktor){
    var AB = Vector.fromPoints(A, B);
    var M = A.addVec(AB.clone().multiply(faktor));

    var c = AB.length();
    var q = AB.clone().multiply(faktor).length();
    var p = c - q;

    var h = Math.sqrt( p*q );

    var MP = AB.vertical().unitVec().multiply(h);

    return M.addVec(MP);
  }

});
