let darkmodeLinkTag = null;
const changeDarkMode = (state) => {
  if (state) {
    if (darkmodeLinkTag == null) {
      darkmodeLinkTag = document.createElement('link');
      darkmodeLinkTag.rel = 'stylesheet';
      darkmodeLinkTag.href = '/assets/darkmode.css';
      let head = document.querySelector('head');
      head.appendChild(darkmodeLinkTag);
    }

    darkmodeLinkTag.disabled = false;
    window.localStorage.setItem('darkmode', 'true');
  } else {
    if (darkmodeLinkTag)
      darkmodeLinkTag.disabled = true;
    window.localStorage.setItem('darkmode', 'false');
  }
};

let checkCheckbox = null;
if (window.localStorage.getItem('darkmode') == 'true') {
  changeDarkMode(true);
  checkCheckbox = true;
} else if (window.localStorage.getItem('darkmode') == 'false') {
  checkCheckbox = false;
}

window.onload = () => {
  const darkmodeCheckbox = document.querySelector('#darkmode-checkbox');
  darkmodeCheckbox.addEventListener('change', evt =>
    changeDarkMode(evt.target.checked));

  if (checkCheckbox == true)
    darkmodeCheckbox.checked = true;
  else if (checkCheckbox == false)
    darkmodeCheckbox.checked = false;
  else {
    if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
      changeDarkMode(true);
      darkmodeCheckbox.checked = true;
    }
  }
};
