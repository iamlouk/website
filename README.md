# My Personal Website

It is build using [pandoc](https://pandoc.org/MANUAL.html).
Run `./build.py` to render the markdown to html.
A *very* basic http-server written in go can run using:
`go run http-server/http-server.go --www ./www --port 80 --data ./data`.
Instead of all those command line arguments, you can use the
[config.yaml](./example-config.yaml) file instead.

